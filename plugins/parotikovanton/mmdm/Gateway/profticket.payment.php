<?php

/**
 * Клиенты систем оплаты
 */
require_once __DIR__ . '/payment_systems/PaymentSystem.php';
require_once __DIR__ . '/payment_systems/Platron.php';
require_once __DIR__ . '/payment_systems/CloudPayments.php';

/**
 * Класс для генерации исключений оплаты
 */
class ProfTicketPaymentManagerException extends Exception
{

}

/**
 * Класс для работы с системами оплаты
 */
class ProfTicketPaymentManager
{
    /**
     * Конфигурации всех платежных систем
     * @var array
     */
    private static $systemConfigs;

    /** @var ProfTicketPaymentSystem[] */
    private static $_paymentSystems;

    /**
     * Возвращает объект платежной системы
     * @param string $paymentSystemCode
     * @return ProfTicketPaymentSystem
     */
    public static function paymentSystem($paymentSystemCode = null)
    {
        // определяем платежную систему из настроек
        $system = $paymentSystemCode ? $paymentSystemCode : variable_get('profticket_payment_system_code', '');

        if (!isset(self::$_paymentSystems[$system])) {
            $config = isset(self::$systemConfigs[$system]) ? self::$systemConfigs[$system] : array();
            if ($system === 'CloudPayments') {
                $paymentSystemClass = 'ProfTicketCloudPaymentsPaymentSystem';
            } else {
                $paymentSystemClass = 'ProfTicketPlatronPaymentSystem';
            }
            self::$_paymentSystems[$system] = new $paymentSystemClass($config);
        }
        return self::$_paymentSystems[$system];
    }

    /**
     * Сеттер настроек платежных систем
     * @param $config
     */
    public static function setConfigs($config) {
        self::$systemConfigs = $config;
    }

    /**
     * Начало оплаты
     * @param ProfTicketOrder $order
     * @return array
     * @throws ProfTicketPaymentManagerException
     */
    public static function startPaying(ProfTicketOrder $order)
    {
        $system = self::paymentSystem();

        ProfTicketLog::log('Начало оплаты, ' . $system::getSystemName() . ' заказ №' . $order->id, null, ProfTicketLog::INFO, $order->id);

        // устанавливаем платежную систему
        $order->payment_system_code = $system::getSystemName();
        if (!$order->save()) {
            ProfTicketLog::log('Невозможно сохранить заказ №' . $order->id, null, ProfTicketLog::ERROR, $order->id);
            throw new ProfTicketPaymentManagerException('Cant save order');
        }

        $canSold = ProfTicketGate::checkSold($order);
        if (!$canSold) {
            ProfTicketLog::log('Невозможно продать билеты, заказ №' . $order->id, null, ProfTicketLog::ERROR, $order->id);
            throw new ProfTicketPaymentManagerException('Cant sold tickets');
        }

        $description = self::getPaymentDescription($order);

        // начинаем транзакцию
        return $system->startPaying($order, $description);
    }

    /**
     * Описание для транзакции
     * @param ProfTicketOrder $order
     * @return string
     */
    public static function getPaymentDescription(ProfTicketOrder $order)
    {
        //получаем мероприятие
        $event = null;
        foreach ($order->getTickets() as $ticket) {
            $event = new ProfTicketEvent($ticket['NomBilKn'], true);
            break;
        }
        $description = 'Мероприятие ' . $event->node->title . ', Дата и время: ' . $event->EventDate . ' ' . substr($event->EventTime, 0, 5) . ', Билетов – ' . count($order->getTickets()) . ' шт.';
        $description = mb_strlen($description) > 250 ? mb_substr($description, 0, 250) . '...' : $description;
        return $description;
    }

    /**
     * Проведение оплаты
     * @param ProfTicketOrder $order
     * @return bool
     */
    public static function processOrder(ProfTicketOrder $order)
    {
        if ($order->isLocked()) {
            //заказ заблокирован
            ProfTicketLog::log("Заказ №{$order->id} заблокирован", null, ProfTicketLog::INFO, $order->id);
            return false;
        }
        $order->lock();

        try {
            $system = self::paymentSystem($order->getPaymentSystemCode());
            ProfTicketLog::log('Проверка оплаты, ' . $system::getSystemName() . ' заказ №' . $order->id, null, ProfTicketLog::INFO, $order->id);
            $system->processTransaction($order);
        } catch (\Exception $e) {
            ProfTicketLog::log('Ошибка проверки оплаты, заказ №' . $order->id . ': ' . $e->getMessage(), null, ProfTicketLog::INFO, $order->id);
        }

        $order->unLock();
    }

    /**
     * Отмена оплаты
     * @param ProfTicketOrder $order
     * @return boolean
     */
    public static function refundOrder(ProfTicketOrder $order)
    {
        $result = false;
        try {
            $system = self::paymentSystem($order->getPaymentSystemCode());
            ProfTicketLog::log('Отмена оплаты, ' . $system::getSystemName() . ' заказ №' . $order->id, null, ProfTicketLog::INFO, $order->id);
            $result = $system->refundTransaction($order);
        } catch (\Exception $e) {
            ProfTicketLog::log('Ошибка отмены оплаты, заказ №' . $order->id . ': ' . $e->getMessage(), null, ProfTicketLog::INFO, $order->id);
        }
        return $result;
    }
}
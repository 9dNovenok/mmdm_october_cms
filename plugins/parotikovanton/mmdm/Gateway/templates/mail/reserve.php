<h2>Уважаемый(ая) <?php print $order->name; ?>!</h2>
<p>Ваш заказ №<?php print $order->id; ?> забронирован.</p>
<p>Благодарим Вас, что Вы воспользовались услугами нашего сайта.</p>
<p><strong>Номер брони: <?php print $order->reservation_id; ?></strong></p>
<h3>Состав заказа:</h3>
<?php
$groupedTickets = array();
foreach($order->getTickets() as $ticket) {
    $groupedTickets[$ticket['NomBilKn']][$ticket['sec_name']][$ticket['row']][] = $ticket;
}

include '_order_tickets.php'; ?>

<?php if (!empty($_SESSION['RESERV_DATETIME'])) : ?>
<strong>Забронированный заказ Вы можете выкупить в кассах Московского международного Дома музыки по адресу: Космодамианская набережная, дом 52, строение 8, не ранее чем через 1 час после момента бронирования. Дата и время окончания брони: <?=$_SESSION['RESERV_DATETIME']?>.</strong>
<?php endif; ?>
<h2>Уважаемый(ая) <?php print $order->name; ?>!</h2>
<p>Ваш заказ №<?php print $order->id; ?> оплачен, транзакция №<?php print $order->payment_system_order_id; ?>.</p>
<p>Благодарим Вас, что Вы воспользовались услугами нашего сайта.</p>
<p><strong>Состав заказа:</strong></p>
<?php
$groupedTickets = array();
foreach($order->getTickets() as $ticket) {
    $groupedTickets[$ticket['NomBilKn']][$ticket['sec_name']][$ticket['row']][] = $ticket;
}

include '_order_tickets.php'; ?>

<?php
/**
 * Список билетов заказа
 */

// заказ с комплектами
if ($order->getSetId()) : ?>
    <?php
    $n = 0;

    // сортируем события по дате
    $sortedEvents = array();
    foreach($groupedTickets as $eventId => $sectors) {
        $timeKey = time() . uniqid();

        if (!isset($events[$eventId])) {
            $events[$eventId] =  new ProfTicketEvent($eventId);
        }

        try {
            $eventTime = DateTime::createFromFormat('d.m.Y H:i:s', $events[$eventId]->EventDate . ' ' . $events[$eventId]->EventTime);
            $eventTime->setTimezone(new DateTimeZone('Europe/Moscow'));
            $timeKey = $eventTime->getTimestamp() . uniqid();
        } catch (\Exception $e) {}

        $sortedEvents[$timeKey] = $events[$eventId];
    }
    ksort($sortedEvents);
    reset($sortedEvents);

    foreach ($sortedEvents as $timeKey => $event) :
        $sumAll = 0;
        $n++;
        ?>

        <?php if ($n === 1) : ?>
            <p>Место проведения: <?php print ProfTicketTools::getFieldValue('field_hall_full', $event->node); ?></p>
            <h4>Мероприятия, входящие в абонемент</h4>
        <?php endif; ?>

        <p>
            Мероприятие: <strong><?php print $event->node->title; ?></strong>
            <br />
            Дата и время: <?php print $event->EventDate; ?> <?php print substr($event->EventTime, 0, 5); ?>
        </p>

    <?php endforeach; ?>

    <?php
    // группируем по комплекту и сектору
    $ticketsBySets = $order->getTicketsBySets();

    $groupedTicketsBySector = array();
    foreach($ticketsBySets as $ticket) {
        $groupedTicketsBySector[$ticket['sec_name']][$ticket['row']][] = $ticket;
    }

        foreach($groupedTicketsBySector as $sector => $rows) : ?>
            <p><strong>Сектор: <?php print $sector; ?></strong><br />

            <?php foreach($rows as $row => $tickets) :
                $sum = 0;
                ?>
                Ряд: <?php print $row; ?><br />
                Места:
                <?php foreach($tickets as $ticket) :
                $sum += $ticket['price'];
                ?>
                <?php print $ticket['seat']; ?>,

            <?php endforeach; ?>
                <br />
                Стоимость: <?php print $sum; ?> р.</p>
        <?php endforeach; ?>

    <?php endforeach; ?>
    <p><strong>Стоимость заказа: <?php print $order->tickets_sum; ?> р.</strong></p>
    <p></p>

<?php else :
    // обычное событие
    foreach($groupedTickets as $eventId => $sectors) :
        $sumAll = 0;
        if (!isset($events[$eventId])) {
            $events[$eventId] =  new ProfTicketEvent($eventId);
        }
        ?>

        <p>Мероприятие: <strong><?php print $events[$eventId]->node->title; ?></strong><br />
            <?php if ($events[$eventId]->node->type == 'events') : ?>
                Дата и время: <?php print $events[$eventId]->EventDate; ?> <?php print substr($events[$eventId]->EventTime, 0, 5); ?><br />
                Место проведения: <?php print ProfTicketTools::getFieldValue('field_hall_full', $events[$eventId]->node); ?>
            <?php else : ?>
                <?php
                //список мероприятий абонемента
                $subEvents = $events[$eventId]->node->field_season_ticket_id[LANGUAGE_NONE];
                foreach ($subEvents as $subEvent) :
                    $nodeId = $subEvent['nid'];
                    $node = node_load($nodeId);
                    if (!$node->field_data_event[LANGUAGE_NONE][0]['value']) {
                        continue;
                    }

                    $eventTime = DateTime::createFromFormat('Y-m-d H:i:s', $node->field_data_event[LANGUAGE_NONE][0]['value'], new DateTimeZone('UTC'));
                    $eventTime->setTimezone(new DateTimeZone('Europe/Moscow'));

                    ?>
                    <span><?php print $eventTime->format('d.m.Y');?> <?php print $eventTime->format('H:i');?>, <?php print ProfTicketTools::getFieldValue('field_hall_full', $node); ?></span><br/>
                <?php endforeach; ?>
            <?php endif; ?>
        </p>

        <?php foreach($sectors as $sector => $rows) : ?>
        <p><strong>Сектор: <?php print $sector; ?></strong><br />

        <?php foreach($rows as $row => $tickets) :
            $sum = 0;
            ?>
            Ряд: <?php print $row; ?><br />
            Места:
            <?php foreach($tickets as $ticket) :
            $sum += $ticket['price'];
            ?>
            <?php print $ticket['seat']; ?>,

        <?php endforeach; ?>
            <br />
            Стоимость: <?php print $sum; $sumAll += $sum; ?> р.</p>
        <?php endforeach; ?>
    <?php endforeach; ?>
        <p><strong>Стоимость заказа: <?php print $sumAll; ?> р.</strong></p>
        <p></p>
    <?php endforeach; ?>
<?php endif; ?>

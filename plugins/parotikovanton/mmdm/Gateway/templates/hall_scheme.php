<?php
/**
 * @param array $scheme
 */

$url = null;
$isSet = false;
if (isset($eventId)) {
    $url = url('/order', array('query' => array(
        'eventId' => $eventId,
        'm' => $isReserve ? 'reserve' : '',
    )));
} elseif (isset($setId)) {
    $isSet = true;
    $url = url('/order', array('query' => array(
        'setId' => $setId,
        'm' => $isReserve ? 'reserve' : '',
    )));
}
?>
<div id="places">
    <div class="scalebox" style="position:absolute;top:0px;left:0px;width:50px;height:50px; font-size: 50px;">
<?php
    foreach ($scheme as $object) :
        $angle = (float) $object['Angle'];
        $y = $object['CY'];
        $x = $object['CX'];
        $w = $object['Width'];
        $h = $object['Height'];
        if ($object['ObjectType'] == 'Place') :
            $eventId = $isSet ? $object['EventsSetID'] : $object['NomBilKn'];
?>

        <div sectionname="<?=$object['Name_sec']?>" row="<?=$object['Row']?>" col="<?=$object['Seat']?>" price=""
                       id="s<?=$object['cod_sec']?>r<?=$object['Row']?>p<?=$object['Seat']?>"
                       class="place status2" IdPlace="<?=$eventId?>-<?=$object['cod_sec']?>-<?=$object['Row']?>-<?=$object['Seat']?>"
                       style="
                               z-index: 10;
                               top: <?=$y?>%;
                               left: <?=$x?>%;
                               width: <?=$w?>%;
                               height: <?=$h?>%;
                               <?php if ($angle) : ?>
                               transform: rotate(<?= $angle ?>deg);
                               <?php endif; ?>
                               ">
        <?php if ($object['Label'] != 0) :
        $xOffset = -25;
        $yOffset = -25;
        //слева
        if ($object['Label'] == 1) {
            $xOffset -= $object['Width'];
            $position = 'right';
            //справа
        } elseif ($object['Label'] == 2) {
            $xOffset += $object['Width'];
            $position = 'left';
            //сверху
        } elseif ($object['Label'] == 3) {
            $yOffset -= $object['Height'];
            $position = 'bottom';
            //снизу
        } elseif ($object['Label'] == 4) {
            $yOffset += $object['Height'];
            $position = 'top';
        }
        $x = ($object['CX'] + $object['CX2']) / 2   + $xOffset;
        $y = ($object['CY'] + $object['CY2']) / 2  + $yOffset;
        ?>
        <span class="row-label" style="<?=$position?>:calc(100% + 3px); display: none;">
            <?= $object['Row'] ?>
        </span>
        <?php endif; ?>
        </div>
        <?php elseif ($object['ObjectType'] == 'Label' && $object['ObjectName'] == 'StageSign') : ?>
            <span class="stage-sign" style="
                    color: white;
                    position: absolute;
                    background: #d5d5d5;
                    text-align: center;
                    font-size: <?= $object['FontSize'] ?>%;
                    top: <?=$y?>%; left: <?=$x?>%; width: <?=$w?>%; height: <?=$h?>%;">
                <span class="text"><?= $object['Name_sec'] ?></span>

        </span>
        <?php elseif ($object['ObjectType'] == 'Label') : ?>
        <span class="scheme-label" style="
                color: black;
                text-align: center;
                position: absolute;
                font-size: <?= $object['FontSize'] ?>%;
                top: <?=$y?>%; left: <?=$x?>%; width: <?=$w?>%; height: <?=$h?>%;">
            <?= $object['Name_sec'] ?>
        </span>
        <?php endif; ?>
        <?php endforeach; ?>

        </div>

</div>
<div id="scalepanel">
    <a href="#" onclick="myplace.scaleplus();return false">
                <span class="zoom-in-block">

                </span>
    </a>
    <a href="#" onclick="myplace.scaleminus();return false">
                <span class="zoom-out-block">

                </span>
    </a>
</div>
<div id="orderbox" class="js-orderbox c-orderbox">
    <form action="<?php print $url ?>" method="POST">
        <div class="hbox">
            <div class="orderbox__more js-orderbox__more">
                <span class="orderbox__more-text js-more-text">
                    Показать еще 3 выбранных места
                </span>
                <span class="orderbox__more-ico chevron-b js-chevron-b"></span>
            </div>
            <div class="orderbox__list js-orderbox__list"></div>

            <div class="hplaces" style="visibility: hidden;"></div>

            <button type="submit" class="orderbox__btn btn btn--order btn-default js-btn js-event-one-click">
                <?php  print $isReserve ? t('Reserve') : t('Purchase'); ?>
                <span class="orderbox__count">0</span>
                <span class="orderbox__ticket js-orderbox__ticket">билетов</span> на
                <span class="orderbox__all-price">0</span>
                <span class="rubl">Р</span>
                <div class="btn-loader">
                    <div class="btn-loader__dots">
                        <span class="btn-loader__item" id="circleG_1"></span>
                        <span class="btn-loader__item" id="circleG_2"></span>
                        <span class="btn-loader__item" id="circleG_3"></span>
                    </div>
                </div>
            </button>

        </div>
    </form>
</div>
<div class="item-template" style="display: none;">
    <div class="orderbox__item">
        <div class="orderbox__right">
            <div class="orderbox__price">21 800</div>
            <span class="rubl orderbox__rubl">Р</span>
            <span class="orderbox__remove ico-remove js-ico-remove"></span>
        </div>
        <div class="orderbox__left">
            <div class="orderbox__seat">Ряд <span class="orderbox__row">19</span>, место <span class="orderbox__col">17</span></div>
            <span class="orderbox__type">Партер</span>
        </div>
    </div>
</div>
<script type="text/javascript">
    /*
     * Лоадер
     * */
    jQuery(document).ready(function(){
        // блокировка кнопки
        jQuery('.js-event-one-click').on('click', function(){
            if (jQuery(this).hasClass('js-clicked')) {
                return false;
            }
            jQuery(this).addClass('btn-loading');
            jQuery('.js-event-one-click').addClass('js-clicked');
        });
    });
</script>

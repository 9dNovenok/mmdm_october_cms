<?php
/** @var $event ProfTicketEvent */
/** @var  $qrCodeFile string */
/** @var $ticket array */

$barcodeType = 'I25';
if ($ticket['BarCodeType'] == 'bcCode_2_5_interleaved') {
    $barcodeType = 'I25';
}
$event->Age = $event->Age > 0 ? $event->Age : 0;
$imagePath = $_SERVER['DOCUMENT_ROOT'] . '/sites/all/modules/profticket/templates/e-ticket/img/';

$footerImage = $imagePath . 'footer.jpg';
$eventImage = $event->getTicketImage();
if ($eventImage) {
    $footerImage = $eventImage;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>Untitled Document</title>

    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }
    </style>

</head>
<body style="width: 2480px; height: 3508px; position: relative; font-family:  Arial, sans-serif; line-height: 1.2">
<div style="position: absolute; left: 57px; top: 0; margin: 0; display: block;">
    <img src="<?= $imagePath ?>header<?= $ticket['set_id'] > 0 ? '_set' : ''?>.jpg" width="2376" height="255" alt=""/>
</div>
<div style="font-size: 39px;position: absolute; right:0; top: 270px; width: 756px; height: 236px;  rotate:-90; text-align: center;">
    <barcode code="<?php print $ticket['barcode']; ?>" type="<?php print $barcodeType; ?>"><br>
        <?php print $ticket['barcode']; ?>
    </barcode>
</div>
<div style="position: absolute; left: 140px; top: 344px; margin: 0; display: block;">
    <?php if ($qrCodeFile) : ?>
    <img src="<?= $qrCodeFile ?>" height="198" alt=""/>
    <?php endif; ?>
</div>
<div style="width: 300px;position: absolute; right: 177px; top: 344px; margin: 0; display: block;font-size: 104px;
color: #000;">
    <?= $event->Age ?>+
</div>
<div style="font-weight: bold;font-size: 67px;padding: 0;position: absolute;left: 382px;top: 338px; margin: 0; display: block;color: #000;width: 1509px;line-height: 1;">
    <?php print $event->node->title; ?>
    <?php if(isset($event->node->OrganizerName)){print '</br>Организатор мероприятия:'.$event->node->OrganizerName;}?>
    <?php if(isset($event->node->OrganizerTin)){print '</br>ИНН:'. $event->node->OrganizerTin;} ?>
    <?php if(isset($event->node->OrganizerAddress)){print '</br>Адрес:'. $event->node->OrganizerAddress;} ?>
    </br>
</div>
<div style="font-weight: normal;font-size: 56px;padding: 0;position: absolute;left: 140px;top: 603px; margin: 0; display: block;color: #000;width: 1509px;line-height: 1;">
    <span style="font-weight: bold;"><?php print $event->EventDate; ?></span> | <?php print substr($event->EventTime, 0, 5); ?>
</div>
<div style="position: absolute; left: 144px; top: 738px; margin: 0; display: block;">
    <img src="<?= $imagePath ?>user.png" width="58" height="58" alt=""/>
</div>
<div style="font-weight: bold;font-size: 50px;padding: 0;position: absolute;left: 226px;top: 746px; margin: 0; display: block;color: #000;width: 1509px;line-height: 1;">
    <?php print $order->name; ?>
</div>
<div style="font-weight: normal;font-size: 33px;padding: 0;position: absolute;left: 139px;top: 872px; display: block;color: #000;width: 800px;line-height: 1.2;">
    <div style="padding-bottom: 29px;">E-mail: <?php print $order->user_email; ?></div>
    <div style="padding-bottom: 29px;">Тел.: <?php print $order->phone; ?></div>
    <div style="padding-bottom: 29px;">№ заказа: <?php print $order->id; ?></div>
    <div style="padding-bottom: 29px;">№ транзакции: <?php print $order->payment_system_order_id; ?></div>
</div>
<div style="font-weight: normal;padding: 0;position: absolute;left: 1047px;top: 822px; display: block;color: #000;width: 1100px;line-height: 1.2;">
    <div style="padding-bottom: 19px;font-size: 50px;"><?php print $event->name_h; ?></div>
    <div style="padding-bottom: 6px;font-size: 42px;font-style: italic;"><?php print $ticket['sec_name']; ?></div>
    <div style="padding-bottom: 11px;font-size: 50px;">ряд <span style="font-weight: bold;margin: 0 45px;font-size: 67px;"><?php print $ticket['row']; ?></span> место <span style="font-weight: bold;margin: 0 18px;font-size: 67px;"><?php print $ticket['seat']; ?></span></div>
    <div style="padding-bottom: 19px;font-size: 50px;">стоимость <span style="font-weight: bold;font-size: 67px;margin-left: 55px;"><?php print $ticket['price']; ?> р.</span></div>
</div>
<div style="position: absolute;left: 0;top: 1149px;width: 100%;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>line.png" alt="" width="100%" height="24"/>
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 188px;top: 1210px; margin: 0; display: block;color: #000;width: 935px;text-align: justify;line-height: 1.2;">
    Билет действителен только при предъявлении на бумажном носителе формата А4
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 188px;top: 1318px; margin: 0; display: block;color: #000;width: 935px;text-align: justify;line-height: 1.2;">
    Для использования в электронной системе сложите билет по линиям сгиба.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 188px;top: 1425px; margin: 0; display: block;color: #000;width: 935px;text-align: justify;line-height: 1.2;">
    По билетам на дневные мероприятия дети допускаются с  3х лет, а на вечерние мероприятия с 7ми лет.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 188px;top: 1425px; margin: 0; display: block;color: #000;width: 935px;text-align: justify;line-height: 1.2;">
    После 3-го звонка зритель  в зрительный зал не допускается и не имеет права на компенсацию стоимости билета.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 188px;top: 1425px; margin: 0; display: block;color: #000;width: 935px;text-align: justify;line-height: 1.2;">
    Билет требует бережного хранения. Берегите билет от копирования. Билет с нечитаемым штрих-кодом недействителен.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 188px;top: 1585px; margin: 0; display: block;color: #000;width: 935px;text-align: justify;line-height: 1.2;">
    Повторный проход по билету невозможен.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 188px;top: 1643px; margin: 0; display: block;color: #000;width: 935px;text-align: justify;line-height: 1.2;">
    Дом музыки  оставляет за собой право отказать в проходе на мероприятие всем предъявителям билетов с одинаковым штрих-кодом.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 188px;top: 1803px; margin: 0; display: block;color: #000;width: 935px;text-align: justify;line-height: 1.2;">
    Билет не может быть передан третьим лицам.     С  условиями оферты можно ознакомиться на сайте  <b>www.profticket.ru</b>.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 188px;top: 1962px; margin: 0; display: block;color: #000;width: 935px;text-align: justify;line-height: 1.2;">
    Настоящий  электронный билет не является офертой.
</div>
<div style="position: absolute;left: 143px;top: 1225px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 143px;top: 1334px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 143px;top: 1437px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 143px;top: 1598px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 143px;top: 1657px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 143px;top: 1815px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 143px;top: 1975px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 1382px;top: 1210px; margin: 0; display: block;color: #000;width: 945px;text-align: justify;line-height: 1.2;">
    The ticket is only valid if presented on A4 paper.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 1382px;top: 1266px; margin: 0; display: block;color: #000;width: 945px;text-align: justify;line-height: 1.2;">
    For using the ticket in electronic system - fold it along the lines.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 1382px;top: 1366px; margin: 0; display: block;color: #000;width: 945px;text-align: justify;line-height: 1.2;">
    Tickets for day activities are allowed for children from 3 years of age, and for evening activities from the age of 7.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 1382px;top: 1366px; margin: 0; display: block;color: #000;width: 945px;text-align: justify;line-height: 1.2;">
    After the third bell, the viewer is not admitted to the auditorium and is not entitled to compensation for the ticket price.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 1382px;top: 1366px; margin: 0; display: block;color: #000;width: 945px;text-align: justify;line-height: 1.2;">
    The ticket requires careful storage. Protect the ticket from copying. The ticket with unreadable bar codes isn't valid.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 1382px;top: 1520px; margin: 0; display: block;color: #000;width: 945px;text-align: justify;line-height: 1.2;">
    Repeated pass by the same ticket is impossible.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 1382px;top: 1575px; margin: 0; display: block;color: #000;width: 945px;text-align: justify;line-height: 1.2;">
    The organizer reserves the right to refuse all ticket owners with an identical bar code to go to an event.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 1382px;top: 1679px; margin: 0; display: block;color: #000;width: 945px;text-align: justify;line-height: 1.2;">
    The ticket can't be transferred to the third parties. You can read all conditions of the offer at <b>www.profticket.ru</b>.
</div>
<div style="font-weight: normal;font-size: 40px;padding: 0;position: absolute;left: 1382px;top: 1833px; margin: 0; display: block;color: #000;width: 945px;text-align: justify;line-height: 1.2;">
    This e-ticket does not constitute an offer.
</div>
<div style="position: absolute;left: 1336px;top: 1225px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 1336px;top: 1276px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 1336px;top: 1382px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 1336px;top: 1536px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 1336px;top: 1589px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 1336px;top: 1693px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 1336px;top: 1846px;width: 24px;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>marker.png" alt="" width="24" height="24"/>
</div>
<div style="position: absolute;left: 1579px;top: 1949px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>rules.png" alt="" width="437" height="83"/>
</div>
<div style="font-size: 39px;position: absolute; left:29px; top: 2135px; width: 778px; height: 236px;  text-align: center;">
    <barcode code="<?php print $ticket['barcode']; ?>" type="<?php print $barcodeType; ?>"><br>
        <?php print $ticket['barcode']; ?>
    </barcode>
</div>
<div  style="font-weight: normal;font-size: 33px;padding: 0;position: absolute;left: 976px;top: 2132px; margin: 0; display: block;color: #000;width: 1400px;text-align: center;line-height: 1.5;">
    <div style="padding-bottom: 16px;">С Правилами посещения Дома музыки, а также с Правилами возврата билетов и
        абонементов можно ознакомиться на официальном сайте Дома музыки www.mmdm.ru</div>
    <div style="color: #db0c16;"><b>По техническим вопросам обращайтесь в Службу поддержки: 8 (495) 215-0000</b></div>
</div>


<div style="position: absolute;left: 0;bottom: 1145px;width: 100%;height: 24px;line-height: 0;">
    <img style="vertical-align: middle;" src="<?= $imagePath ?>line.png" alt="" width="100%" height="24"/>
</div>
<div style="position: absolute; left: 65px; bottom: 30px; margin: 0; display: block;">
    <img src="<?= $footerImage ?>" width="2343" height="1084" alt=""/>
</div>
</body>
</html>
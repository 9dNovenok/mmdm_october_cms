<?php


/**
 * Класс для генерации исключений
 */
class ProfTicketPlatronPaymentSystemException extends ProfTicketPaymentSystemException
{

}

/**
 * Класс работы с платежной системой Platron
 */
class ProfTicketPlatronPaymentSystem extends ProfTicketPaymentSystem
{

    /**
     * Название платежной системы
     */
    const SYSTEM_NAME = 'Platron';

    /**
     * ID магазина
     * @return mixed
     */
    public function getMerchantId()
    {
        return $this->config['pg_merchant_id'];
    }

    /**
     * Создание транзакции в платроне
     * @param ProfTicketOrder $order
     * @param string $description
     * @return array
     */
    public function startPaying(ProfTicketOrder $order, $description)
    {
        ProfTicketLog::log('Формируем данные для платрона №' . $order->id, null, ProfTicketLog::INFO, $order->id);

        $reqParams = array(
            'pg_merchant_id' => $this->config['pg_merchant_id'],
            'pg_order_id' => $order->id,
            'pg_amount' => $order->tickets_sum,
            'pg_check_url' => '',
            'pg_result_url' => $this->config['pg_result_url'],
            'pg_request_method' => $this->config['pg_request_method'],
            'pg_success_url' => $this->config['pg_success_url'],
            'pg_failure_url' => $this->config['pg_failure_url'],
            'pg_success_url_method' => $this->config['pg_success_url_method'],
            'pg_failure_url_method' => $this->config['pg_failure_url_method'],
            'pg_lifetime' => 60 * 60, //час
            'pg_site_url' => 'http://' . $_SERVER['SERVER_NAME'],
            'pg_description' => $description,
            'pg_user_phone' => $order->phone,
            'pg_user_contact_email' => $order->user_email,
            'pg_testing_mode' => $this->config['pg_testing_mode'],
            'pg_salt' => self::getSalt(),
        );
        $reqParams['pg_sig'] = $this->makeSigStr('payment.php', $reqParams, $this->config['secretKey']);

        $url = $this->config['pay_url'] . '?' . http_build_query($reqParams);
        ProfTicketLog::log('Отправляем в платрон: ' . $url, null, ProfTicketLog::INFO, $order->id);
        $params = array();
        $params['url'] = $this->config['pay_url'];
        $params['data'] = $reqParams;
        return $params;
    }

    /**
     * Отмена транзакции
     * @param ProfTicketOrder $order
     * @throws ProfTicketPlatronPaymentSystemException
     * @return true в случае успеха
     */
    public function refundTransaction(ProfTicketOrder $order)
    {
        ProfTicketLog::log('Пытаемся отменить оплату заказа №' . $order->id, null, ProfTicketLog::WARNING, $order->id);

        $reqParams = array(
            'pg_merchant_id' => $this->config['pg_merchant_id'],
            'pg_payment_id' => $order->payment_system_order_id,
            'pg_salt' =>  self::getSalt()
        );
        $reqParams['pg_sig'] = self::makeSigStr('revoke.php', $reqParams, $this->config['secretKey']);

        $url = $this->config['pg_revoke_url'] . '?' . http_build_query($reqParams);

        try {
            $res = file_get_contents($url);

            if (trim($res)) {
                $res = (array)simplexml_load_string($res);
                // все переменные массива переводим в строки если это объекты
                foreach ($res as $k => $v) {
                    if (is_object($v)) {
                        $res[$k] = (string)$v;
                    }
                }
                // проверка сигнатуры
                $checkSig = self::checkSigStr($res['pg_sig'], 'revoke.php', $res, $this->config['secretKey']);
                if (!$checkSig) {
                    ProfTicketLog::log('Ошибка получения статуса заказа №' . $order->id, null, ProfTicketLog::WARNING, $order->id);
                    throw new ProfTicketPlatronPaymentSystemException('Ошибка получения статуса заказа');
                }

                //ошибка возврата средств
                if ($res['pg_status'] == 'error') {
                    ProfTicketLog::log('Ошибка отмены оплаченного заказа №' . $order->id, null, ProfTicketLog::ERROR, $order->id);

                    //уведомляем администратора
                    drupal_mail('system', 'mail', PROFTICKET_ADMIN_EMAIL, language_default(), array(
                        'context' => array(
                            'subject' => 'Ошибка отмены оплаченного заказа',
                            'message' => 'Ошибка отмены оплаченного заказа №' . $order->id . '. Подробнее в логах модуля.',
                        )
                    ));
                    throw new ProfTicketPlatronPaymentSystemException('Ошибка отмены оплаченного заказа');
                }

            }
        } catch (Exception $e) {
            throw new ProfTicketPlatronPaymentSystemException($e->getMessage(), $e->getCode(), $e);
        }

        return true;
    }

    /**
     * Проверка статуса заказа
     * @param ProfTicketOrder $order
     * @return bool
     * @throws ProfTicketPlatronPaymentSystemException
     */
    public function processTransaction(ProfTicketOrder $order)
    {
        /**
         * платрон возвращает статусы в поле pg_transaction_status
         *
         * partial платежная транзакция еще не до конца создана, например, не определена
        платежная система. Из этого состояния платеж может перейти только в состояние
        pending.
         *
        pending платежная транзакция создана и ждет оплаты. Из этого состояния платеж может
        перейти только в состояния ok или failed.
         *
        ok платеж завершился успешно. Из этого состояния платеж может перейти только в
        состояние revoked.
         *
        failed платеж не прошел. Это окончательный статус.
         *
        revoked платеж прошел успешно, но затем был отозван. Это окончательный статус.
         *
         * нас интересует только ok
         */

        $ret = array();

        // формирование запроса
        $reqParams = array(
            'pg_merchant_id' => $this->config['pg_merchant_id'],
            'pg_order_id' => $order->id,
            'pg_salt' => self::getSalt(),
        );
        $reqParams['pg_sig'] = self::makeSigStr('get_status.php', $reqParams, $this->config['secretKey']);

        $url = $this->config['status_url'] . '?' . http_build_query($reqParams);

        try {
            $res = file_get_contents($url);
            if (trim($res)) {
                $res = (array) simplexml_load_string($res);
                // все переменные массива переводим в строки если это объекты
                foreach ($res as $k => $v) {
                    if (is_object($v)) {
                        $res[$k] = (string) $v;
                    }
                }

                // проверка сигнатуры
                $checkSig = self::checkSigStr($res['pg_sig'], 'get_status.php', $res, $this->config['secretKey']);
                if ($checkSig) {
                    // сигнатура верна, возвращаем ответ
                    $ret = $res;
                }
            }
        }
        catch (Exception $e) {
            $ret = array();
        }

        $status = false;

        ProfTicketLog::log("Заказ №{$order->id}, данные транзакции: " . json_encode($ret), null, ProfTicketLog::INFO, $order->id);

        try {
            // обновляем данные заказа
            if (isset($ret['pg_payment_id']) && !$order->payment_system_order_id) {
                $order->payment_system_order_id = $ret['pg_payment_id'];
            }
            if (isset($ret['pg_transaction_status'])) {
                if ($ret['pg_transaction_status'] == 'ok') {
                    $status = true;
                    if ($order->payment_status_id != ProfTicketOrder::STATUS_PAID) {
                        ProfTicketLog::log("Заказ №{$order->id} оплачен", null, ProfTicketLog::INFO, $order->id);

                        //установка продажи в шлюзе
                        $order->setSold();
                    }

                } elseif ($ret['pg_transaction_status'] == 'pending') {
                    $order->payment_status_id = ProfTicketOrder::STATUS_PENDING;

                    //ошибка оплаты - откатываем бронь\продажу
                } elseif ($ret['pg_transaction_status'] == 'failed') {
                    ProfTicketLog::log("Заказ №{$order->id} - ошибка оплаты", null, ProfTicketLog::WARNING, $order->id);
                    $order->rollBack();
                    $order->payment_status_id = ProfTicketOrder::STATUS_PAYMENT_ERROR;

                    //платеж отменен - откатываем бронь\продажу
                } elseif ($ret['pg_transaction_status'] == 'revoked') {
                    ProfTicketLog::log("Заказ №{$order->id} был отменен", null, ProfTicketLog::WARNING, $order->id);
                    $order->rollBack();
                    $order->payment_status_id = ProfTicketOrder::STATUS_ROLLBACK;

                }
            }

            $order->save();
        } catch (\Exception $e) {
            throw new ProfTicketPlatronPaymentSystemException($e->getMessage(), $e->getCode(), $e);
        }

        return $status;
    }

    /**
     * создание подписи для запроса
     * @param string $strScriptName имя скрипта
     * @param array $arrParams массив параметров запроса
     * @param string $strSecretKey пароль продавца
     * @return string
     */
    public function makeSigStr($strScriptName, $arrParams, $strSecretKey = null)
    {
        $config = $this->config;
        if (!$strSecretKey) {
            $strSecretKey = $config['secretKey'];
        }
        if (isset($arrParams['pg_sig'])) unset($arrParams['pg_sig']);

        ksort($arrParams);

        array_unshift($arrParams, $strScriptName);
        array_push   ($arrParams, $strSecretKey);

        return md5(implode(';', $arrParams));
    }

    /**
     * Проверка подписи для запроса
     *
     * @param string $sig подпись для проверки
     * @param string $strScriptName имя скрипта
     * @param array $arrParams массив параметров запроса
     * @param string $strSecretKey пароль продавца
     * @return bool
     */
    public function checkSigStr($sig, $strScriptName, $arrParams, $strSecretKey)
    {
        return $sig === $this->makeSigStr($strScriptName, $arrParams, $strSecretKey);
    }

    /**
     * Сформировать ответ и вывести в поток
     *
     * @param string $strScriptName имя скрипта
     * @param string $strStatus статус ответа: ok - успех, error - ошибка
     * @param string $strDescription описание ошибки в случае $strStatus=error
     */
    public static function makeResponse($strScriptName, $strStatus, $strDescription = '')
    {
        $arrResponse = array();
        $arrResponse['pg_salt'] = self::getSalt();
        $arrResponse['pg_status'] = $strStatus;
        $arrResponse['pg_description'] = $strDescription;
        $arrResponse['pg_sig'] = self::makeSigStr($strScriptName, $arrResponse);

        header("Content-Type: text/xml");
        header("Pragma: no-cache");
        $strResponse = "<"."?xml version=\"1.0\" encoding=\"utf-8\"?".">\n";
        $strResponse .= "<response>";
        $strResponse .= "<pg_salt>".$arrResponse['pg_salt']."</pg_salt>";
        $strResponse .= "<pg_status>".$arrResponse['pg_status']."</pg_status>";
        $strResponse .= "<pg_description>".$arrResponse['pg_description']."</pg_description>";
        $strResponse .= "<pg_sig>".$arrResponse['pg_sig']."</pg_sig>";
        $strResponse .= "</response>";
        echo $strResponse;
        exit();
    }

    /**
     * Соль
     * @return int
     */
    public static function getSalt()
    {
        return rand(21, 43433);
    }
}
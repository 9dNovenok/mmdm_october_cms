<?php


/**
 * Класс для генерации исключений
 */
class ProfTicketPaymentSystemException extends Exception
{

}

/**
 * Базовый класс систем оплаты
 * Все системы оплаты должны от него наследоваться
 */
abstract class ProfTicketPaymentSystem
{
    /**
     * Название платежной системы
     */
    const SYSTEM_NAME = '';

    /**
     * @var array конфигурация платежной системы
     */
    protected $config = array();

    /**
     * Конструктор
     * @param array $config параметры платежной системы
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Название платежной системы
     * @return string
     */
    public static function getSystemName()
    {
        return static::SYSTEM_NAME;
    }

    /**
     * Отмена транзакции заказа
     * @param ProfTicketOrder $order
     * @return mixed
     */
    abstract function refundTransaction(ProfTicketOrder $order);

    /**
     * Проведение транзакции заказа
     * @param ProfTicketOrder $order
     * @return mixed
     */
    abstract function processTransaction(ProfTicketOrder $order);

    /**
     * ID магазина
     * @return mixed
     */
    abstract function getMerchantId();

    /**
     * Начало работы с заказом
     * @param ProfTicketOrder $order
     * @param string $description
     * @return mixed
     */
    abstract function startPaying(ProfTicketOrder $order, $description);
}
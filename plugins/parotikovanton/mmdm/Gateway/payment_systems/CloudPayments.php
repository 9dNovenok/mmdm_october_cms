<?php


/**
 * Класс для генерации исключений
 */
class ProfTicketCloudPaymentsPaymentSystemException extends ProfTicketPaymentSystemException
{

}

/**
 * Класс работы с платежной системой CloudPayments
 * Class ProfTicketCloudPaymentsPaymentSystem
 */
class ProfTicketCloudPaymentsPaymentSystem extends ProfTicketPaymentSystem
{

    /**
     * Название платежной системы
     */
    const SYSTEM_NAME = 'CloudPayments';

    /**
     * Хост АПИ
     */
    const API_HOST = 'https://api.cloudpayments.ru/';


    /**
     * Редирект на ввод карты
     * @param ProfTicketOrder $order
     * @param string $description
     * @return array
     */
    public function startPaying(ProfTicketOrder $order, $description)
    {
        return array(
            'url' => self::createUrl(url('/cloud-payments'), array(
                'orderId' => $order->id,
                'hash' => $order->getHash(),
            )),
            'data' => array()
        );
    }

    /**
     * Проверка результата 3D secure
     * @param ProfTicketOrder $order
     * @param $transactionId
     * @param $params
     * @return string
     * @throws ProfTicketCloudPaymentsPaymentSystemException
     */
    public function process3dSecure(ProfTicketOrder $order, $transactionId, $params)
    {
        $validate = $this->sendRequest('payments/cards/post3ds', [
            'TransactionId' => $transactionId,
            'PaRes' => $params,
        ]);

        $errorMessage = null;

        if (isset($validate['Success']) && $validate['Success']) {
            return self::createUrl($this->config['success_url'], array(
                    'orderId' => $order->id,
                    'hash' => $order->getHash()
                )
            );
        } elseif (isset($validate['Model']['CardHolderMessage'])) {
            $errorMessage = $validate['Model']['CardHolderMessage'];
        }

        return self::createUrl($this->config['failure_url'], array(
                'orderId' => $order->id,
                'hash' => $order->getHash(),
                'error' => base64_encode($errorMessage),
            )
        );
    }

    /**
     * Отмена транзакции
     * если транзакция была завершена, то возвращаем деньги, если нет - откатываем транзакцию
     * @param ProfTicketOrder $order
     * @return bool
     * @throws ProfTicketCloudPaymentsPaymentSystemException
     * @throws ProfTicketOrderException
     */
    public function refundTransaction(ProfTicketOrder $order)
    {
        $paymentResult = $this->findPayment($order->id);
        if (!$paymentResult['Success'] || empty($paymentResult['Model'])) {
            throw new ProfTicketCloudPaymentsPaymentSystemException('Error get transaction');
        }
        $transactionParams = $paymentResult['Model'];

        // если завершена - возвращаем деньги
        if ($transactionParams['Status'] === 'Completed') {
            $result = $this->sendRequest('payments/refund', [
                'TransactionId' => $order->payment_system_order_id,
                'Amount' => $order->tickets_sum
            ]);
        } else {
            $result = $this->sendRequest('payments/void', [
                'TransactionId' => $order->payment_system_order_id,
            ]);
        }

        $ret = isset($result['Success']) && $result['Success'];
        if ($ret) {
            // сохранение отмены транзакции
            $order->payment_status_id = ProfTicketOrder::STATUS_ROLLBACK;
            if (!$order->save()) {
                throw new ProfTicketCloudPaymentsPaymentSystemException('Error save order');
            }
        }
        return $ret;
    }


    /**
     * Проведение платежа
     * Если платеж в статусе Authorized - его нужно подтвердить
     * @param ProfTicketOrder $order
     * @return bool
     * @throws Exception
     * @throws ProfTicketCloudPaymentsPaymentSystemException
     * @throws ProfTicketOrderException
     */
    public function processTransaction(ProfTicketOrder $order)
    {
        $paymentResult = $this->findPayment($order->id);
        if (!$paymentResult['Success'] || empty($paymentResult['Model'])) {
            throw new ProfTicketCloudPaymentsPaymentSystemException('Error get transaction');
        }
        $transactionParams = $paymentResult['Model'];

        $payed = false;

        if ($transactionParams['ReasonCode'] === 0
            && ($transactionParams['Status'] === 'Completed' || $transactionParams['Status'] === 'Authorized')
            && $order->payment_status_id != ProfTicketOrder::STATUS_PAID) {

            if (((int)$transactionParams['Amount']) < ((int)$order->tickets_sum)) {
                throw new ProfTicketCloudPaymentsPaymentSystemException('Error transaction sum ' . $transactionParams['Amount'] . ' for order '  . $order->id);
            }

            $order->payment_system_order_id = (string) $transactionParams['TransactionId'];

            if (!$order->save()) {
                throw new ProfTicketCloudPaymentsPaymentSystemException('Error save order');
            }

            //установка продажи в шлюзе
            $order->setSold();

            // комитим транзакцию, если нужно
            if ($transactionParams['Status'] === 'Authorized') {
                if (!$this->commitTransaction($order)) {
                    $order->rollBack();
                    throw new ProfTicketCloudPaymentsPaymentSystemException('Error commit order transaction');
                }
            }

            $payed = true;
        }

        return $payed;
    }

    /**
     * Подтверждение списания средств
     * @param ProfTicketOrder $order
     * @return bool
     * @throws ProfTicketCloudPaymentsPaymentSystemException
     */
    public function commitTransaction(ProfTicketOrder $order)
    {
        ProfTicketLog::log("Cloud::process - подтверждение оплаты заказа #" . $order->id, null, ProfTicketLog::INFO);
        $result = false;
        $params = array(
            'TransactionId' => $order->payment_system_order_id,
            'Amount' => $order->tickets_sum
        );
        $requestResult = $this->sendRequest('payments/confirm', $params);
        if (isset($requestResult['Success']) && $requestResult['Success']) {
            $result = true;
        }
        return $result;
    }

    /**
     * Создание заказа
     * @param ProfTicketOrder $order
     * @param string $description
     * @param $cryptCardData
     * @param $cardHolderName
     * @param $accountId
     * @return array|mixed
     * @throws ProfTicketCloudPaymentsPaymentSystemException
     */
    public function createOrder(ProfTicketOrder $order, $description, $cryptCardData, $cardHolderName, $accountId = null)
    {
        // формируем описание
        if (preg_match_all('#(([\d]{1,3})\.([\d]{1,3})\.([\d]{1,3})\.([\d]{1,3}))#is', ProfTicketTools::getRealIp(), $matches) && !empty($matches[1])) {
            $ip = array_pop($matches[1]);
        }
        else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $result = [
            'success' => false,
            'url' => '',
            'data' => [],
        ];

        $errorMessage = null;

        $params = [
            'Amount' => $order->tickets_sum,
            'IpAddress' => $ip,
            'Currency' => 'RUB',
            'Name' => $cardHolderName,
            'CardCryptogramPacket' => $cryptCardData,
            'InvoiceId' => $order->id,
            'Description' => $description,
            'Email' => $order->user_email,
            'accountId' => $accountId
        ];
        try {
            ProfTicketLog::log("Cloud::createOrder - блокируем средства на заказ #" . $order->id, null, ProfTicketLog::INFO);
            $requestResult = $this->sendRequest('payments/cards/auth', $params);

            // сохраняем номер транзакции
            if (isset($requestResult['Model']['TransactionId']) && $requestResult['Model']['TransactionId']) {
                $order->payment_system_order_id = (string) $requestResult['Model']['TransactionId'];
            }

            if ($requestResult['Success'] == false) {

                $reasonCode = isset($requestResult['Model']['ReasonCode']) ? $requestResult['Model']['ReasonCode'] : null;

                // оплата отклонена
                if (!is_null($reasonCode) && $reasonCode !== 0) {
                    $errorMessage = $requestResult['Model']['CardHolderMessage'];

                    throw new ProfTicketCloudPaymentsPaymentSystemException('Error create order ' . $order->id . ': ' . $requestResult['Model']['CardHolderMessage']);
                }

                // требуется 3d secure
                $params3dSecure = [];
                if (isset($requestResult['Model']['AcsUrl'])) {
                    $order->payment_status_id = ProfTicketOrder::STATUS_PENDING;

                    $params3dSecure['PaReq'] = $requestResult['Model']['PaReq'];
                    $params3dSecure['MD'] = $requestResult['Model']['TransactionId'];

                    $params3dSecure['TermUrl'] = self::createUrl($this->config['3d_secure_url'], array(
                            'orderId' => $order->id,
                            'hash' => $order->getHash()
                        )
                    );

                    $result['url'] = $requestResult['Model']['AcsUrl'];
                    $result['data'] = $params3dSecure;
                } else {
                    // ошибка
                    throw new ProfTicketCloudPaymentsPaymentSystemException('Error create order ' . $order->id . ': ' . $requestResult['Message']);
                }
            } else {
                // успешно
                $order->payment_status_id = ProfTicketOrder::STATUS_PENDING;
                $result['success'] = true;
                $result['url'] =  self::createUrl($this->config['success_url'], array(
                        'orderId' => $order->id,
                        'hash' => $order->getHash()
                    )
                );
            }
        } catch (\Exception $e) {
            // если заказ забронирован, ошибку не ставим
            if ($order->payment_status_id != ProfTicketOrder::STATUS_RESERVATION) {
                $order->payment_status_id = ProfTicketOrder::STATUS_PAYMENT_ERROR;
            }
            $result['url'] =  self::createUrl($this->config['failure_url'], array(
                    'orderId' => $order->id,
                    'hash' => $order->getHash(),
                    'error' => base64_encode($errorMessage),
                )
            );
        }

        if (!$order->save()) {
            throw new ProfTicketCloudPaymentsPaymentSystemException('Error save order ' . $order->id);
        }

        return $result;
    }

    /**
     * Метод тестирования взаимодействия с апи
     * @return mixed
     */
    public function test()
    {
        return $this->sendRequest('test', []);
    }

    /**
     * Отправка запроса апи
     * @param string $uri
     * @param array $data
     * @return mixed
     *
     * @throws ProfTicketCloudPaymentsPaymentSystemException
     */
    public function sendRequest($uri, $data)
    {
        $fields = http_build_query($data);
        $process = curl_init(self::API_HOST . $uri);
        curl_setopt($process, CURLOPT_USERPWD, $this->config['login'] . ":" . $this->config['secret']);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_POST, 1);
        curl_setopt($process, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, true);
        $return = curl_exec($process);

        $info = curl_getinfo($process);
        if ($info['http_code'] !== 200) {
            throw new ProfTicketCloudPaymentsPaymentSystemException('Error send request code: ' . $info['http_code']);
        }
        curl_close($process);

        ProfTicketLog::log("Cloud::process - Sent request {$uri} {$fields}, response {$return}", null, ProfTicketLog::INFO);

        return json_decode($return, true);
    }

    /**
     * Генерирует урл с гет параметрами
     * @param string $url
     * @param array $params
     * @return string
     */
    public static function createUrl($url, $params = array())
    {
        return $url . '?' . http_build_query($params);
    }

    /**
     * Информация о заказе
     * @param string $orderId
     * @return mixed
     * @throws ProfTicketCloudPaymentsPaymentSystemException
     */
    public function findPayment($orderId)
    {
        return $this->sendRequest('payments/find', [
            'InvoiceId' => $orderId
        ]);
    }

    /**
     * @return mixed
     */
    public function getMerchantId()
    {
        return $this->config['login'];
    }

    /**
     * Коды ошибок, которые определяют причину отказа в проведении платежа.
     *
     * @see https://cloudpayments.ru/Docs/Directory#error-codes
     *
     * @return array
     */
    public function getCancelPaymentsCodes()
    {
        return [
            5001 => 'Отказ эмитента проводить онлайн операцию',
            5005 => 'Отказ эмитента без объяснения причин',
            5006 => 'Отказ сети проводить операцию или неправильный CVV код',
            5012 => 'Карта не предназначена для онлайн платежей',
            5013 => 'Слишком маленькая или слишком большая сумма операции',
            5030 => 'Ошибка на стороне эквайера — неверно сформирована транзакция',
            5031 => 'Неизвестный эмитент карты',
            5034 => 'Отказ эмитента — подозрение на мошенничество',
            5041 => 'Карта потеряна',
            5043 => 'Карта украдена',
            5051 => 'Недостаточно средств',
            5054 => 'Карта просрочена или неверно указан срок действия',
            5057 => 'Ограничение на карте',
            5065 => 'Превышен лимит операций по карте',
            5082 => 'Неверный CVV код',
            5091 => 'Эмитент недоступен',
            5092 => 'Эмитент недоступен',
            5096 => 'Ошибка банка-эквайера или сети',
            5204 => 'Операция не может быть обработана по прочим причинам',
            5206 => '3-D Secure авторизация не пройдена',
            5207 => '3-D Secure авторизация недоступна',
            5300 => 'Лимиты эквайера на проведение операций',
        ];
    }
}

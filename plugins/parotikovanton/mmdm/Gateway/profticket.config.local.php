<?php
/**
 * Конфигурация модуля ProfTicket
 *
 * Здесь указываются:
 * 1) параметры подключения к шлюзу;
 * 2) параметры доступа к платежной системе Platron.
 */

define('PROFTICKET_WSDL', 'http://195.9.173.102:8150/scripts/ProfticketGate.exe/wsdl/IProfTicket');
define('PROFTICKET_USER', 'Gate12');
define('PROFTICKET_PASSWORD', 'Gate156@New23');

//уровень логирования, если не определено - логирования нет
define('PROFTICKET_LOG_LEVEL', 0);

define('PROFTICKET_ADMIN_EMAIL', 'nikitchenko@internet-design.ru');
define('PROFTICKET_FROM_EMAIL', 'bilet@mmdm.ru');


//Platron - платёжная система
$platronConfig = array();
$platronConfig['pg_merchant_id'] = '8892';
$platronConfig['secretKey'] = 'nybogiwivycadewu';
$platronConfig['pg_testing_mode'] = '0';
$platronConfig['pay_url'] = 'http://www.platron.ru/payment.php';
$platronConfig['status_url'] = 'http://www.platron.ru/get_status.php';
$platronConfig['pg_result_url'] = 'https://www.mmdm.ru/pay-result.php';
$platronConfig['pg_revoke_url'] = 'http://www.platron.ru/revoke.php';
$platronConfig['pg_request_method'] = 'GET';
$platronConfig['pg_success_url'] = 'https://www.mmdm.ru/success-order/';
$platronConfig['pg_failure_url'] = 'https://www.mmdm.ru/fail-order/';
$platronConfig['pg_success_url_method'] = 'AUTOGET';
$platronConfig['pg_failure_url_method'] = 'AUTOGET';


// CloudPayments - платежная система
$cloudPaymentsConfig = array();
$cloudPaymentsConfig['login'] = 'pk_b437e644b4cb4bcc3531565c79cd6';//'pk_20b4287c831d3db977ff2f459fb1e';
$cloudPaymentsConfig['secret'] = 'dde8fade6d05b8a8a568f1c62242258d';//'ac0bc3ec5a5d9f3fe8d5238c563623eb';
// test
//$cloudPaymentsConfig['login'] = 'pk_20b4287c831d3db977ff2f459fb1e';
//$cloudPaymentsConfig['secret'] = 'ac0bc3ec5a5d9f3fe8d5238c563623eb';

$cloudPaymentsConfig['success_url'] = 'https://www.mmdm.ru/success-order/';
$cloudPaymentsConfig['failure_url'] = 'https://www.mmdm.ru/fail-order/';
$cloudPaymentsConfig['3d_secure_url'] = 'https://www.mmdm.ru/payment/cloud/check-3d-secure.php';

// массив всех настроек платежных систем
$paymentSystemsConfig = array(
    'Platron' => $platronConfig,
    'CloudPayments' => $cloudPaymentsConfig,
);
unset($platronConfig);
unset($cloudPaymentsConfig);

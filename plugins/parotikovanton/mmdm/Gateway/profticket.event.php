<?php

require_once __DIR__ . '/profticket.gate.php';

/**
 * Класс для генерации исключений событий
 */
class ProfTicketEventException extends Exception
{

}

/**
 * Класс для работы с событиями из шлюза.
 *
 * На вход конструктору передается идентификатор события (NomBilKn).
 */
class ProfTicketEvent
{
    // таблица для хранения событий
    const EVENT_TABLE = 'profticket_event';

    /**
     * @var boolean Включить кеш или нет (удалить этот флаг, когда можно будет реально работать с кешем)
     */
    protected $cacheEnabled = false;

    /**
     * @var array массив полей события из шлюза
     */
    protected $_fields = array();

    /**
     * @var ISTicket_Gate клиент
     */
    public $client;

    /**
     * @var array места с ценами по секторам и рядам
     */
    public static $places;

    /**
     * Конструктор. На вход передается идентификатор события в шлюзе.
     *
     * @param int $nomBilKn
     * @param bool $force
     * @param string $langCode
     * @throws ProfTicketEventException
     */
    public function __construct($nomBilKn, $force = false, $langCode = 'ru')
    {
        $this->client = ProfTicketGate::getInstance();

        //получаем параметры мероприятия из шлюза
//        $event = $this->getEventByNomBilKn($nomBilKn);
//        if (!empty($event)) {
//            $this->_fields = $event;
//        }
//
//        /**
//         * Пытаемся получить мероприятие без мест, если force
//         */
//        if (empty($this->_fields) && $force) {
//            $event = $this->getEventByNomBilKn($nomBilKn, true);
//            if (!empty($event)) {
//                $this->_fields = $event;
//            }
//        }
//
//        if (empty($this->_fields)) {
//            throw new ProfTicketEventException('Empty fields');
//        }

       // $this->_fields['node'] = self::getEventNode($nomBilKn, $langCode);

       // if (empty($this->_fields['node'])) {
       //     throw new ProfTicketEventException('Node not found');
       // }

    }

    /**
     * Получить ноду события
     * @param int $nomBilKn
     * @param string $langCode
     * @return array|null
     */
    public static function getEventNode($nomBilKn, $langCode = 'ru')
    {
        $node = null;
        //получаем параметры мероприятия из базы
        //обычное событие
        $query = new EntityFieldQuery();
        $result = $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'events')
            ->fieldCondition('event_external_id', 'value', $nomBilKn)
            ->propertyCondition('language', $langCode, '=')
            ->execute();

        //абонемент
        $query2 = new EntityFieldQuery();
        $resultSeason = $query2->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'season_ticket')
            ->fieldCondition('field_season_ticket_id_1', 'value', $nomBilKn)
            ->propertyCondition('language', $langCode, '=')
            ->execute();

        //абонемент вторая серия
        $query3 = new EntityFieldQuery();
        $resultSeason2 = $query3->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'season_ticket')
            ->fieldCondition('field_season_ticket_id_2', 'value', $nomBilKn)
            ->propertyCondition('language', $langCode, '=')
            ->execute();

        if (!empty($resultSeason['node'])) {
            $result = $resultSeason;
        }

        if (!empty($resultSeason2['node'])) {
            $result = $resultSeason2;
        }

        if (!empty($result['node'])) {
            $nodeIds = array_keys($result['node']);
            if (count($nodeIds) == 1) {
                $node = node_load($nodeIds[0]);
            }
        }

        return $node;
    }

    /**
     * Картинка для билета, возвращает абсолютный путь
     * @return null|string
     */
    public function getTicketImage()
    {
//        $uri = null;
//        // ищем у мероприятия
//        if (isset($this->_fields['node']->field_ticket_image['und'][0]['uri'])) {
//            $uri = drupal_realpath($this->_fields['node']->field_ticket_image['und'][0]['uri']);
//        }
//
//        // ищем в таксономии
//        if (!$uri) {
//            $query = new EntityFieldQuery();
//            $result = $query->entityCondition('entity_type', 'taxonomy_term')
//                ->entityCondition('bundle', 'ticket_images')
//                ->execute();
//            $list = array();
//            if ($result['taxonomy_term']) {
//                $ids = array_keys($result['taxonomy_term']);
//                $list = entity_load('taxonomy_term', $ids);
//            }
//
//            if (!empty($list)) {
//                shuffle($list);
//                $item = reset($list);
//                if (isset($item->field_ticket_image['und'][0]['uri'])) {
//                    $uri = drupal_realpath($item->field_ticket_image['und'][0]['uri']);
//                }
//            }
//
//
//        }

        return 'http://8dehb.ru/logo.jpg';
    }

    /**
     * Получить массив мероприятия из шлюза
     *
     * @param integer $nomBilKn Идентификатор мероприятия
     * @param boolean $force Попытка получить мероприятие без мест (false по умолчанию)
     * @return array поля мероприятия из шлюза
     */
    protected function getEventByNomBilKn($nomBilKn, $force = false)
    {
        if ($this->cacheEnabled) {
            //получаем параметры мероприятия из шлюза
            $cacheId = $force ? 'GetEventByIdForce_' . $nomBilKn : 'GetEventById_' . $nomBilKn;
            $cacheBin = 'cache';
            $cacheTime = time() + (60 * 15); // на 15 минут
            $cache = cache_get($cacheId, $cacheBin);
            if (isset($cache->data) && isset($cache->expire) && $cache->expire > time()) {
                return $cache->data;
            }
            cache_clear_all($cacheId, $cacheBin);
        }
        $query = array(
            'NomBilKn' => $nomBilKn,
        );
        if ($force) {
            $query['FreeOnly'] = 0;
        }
        $res = $this->client->call('GetEventList', $query, true);
        $cachedEvent = array();
        foreach ($res as $event) {
            if ($event['NomBilKn'] == $nomBilKn) {
                $cachedEvent = $event;
                break;
            }
        }
        if ($this->cacheEnabled) {
            cache_set($cacheId, $cachedEvent, $cacheBin, $cacheTime);
        }
        return $cachedEvent;
    }

    /**
     * Получить поле события
     * @param string $fieldName
     * @return
     */
    public function __get($fieldName)
    {
        return isset($this->_fields[$fieldName]) ? $this->_fields[$fieldName] : null;
    }

    /**
     * Получить схему зала
     * @return array массив объектов зала из шлюза
     */
    public function getHallScheme()
    {
        $places = array();
        if ($this->client) {
            $places = $this->client->call('GetSchemaHallList', array(
                array(
                'NomBilKn' => $this->NomBilKn,
                'ShowLabels' => 1,
                ),
            ), true);
        }

        return $places;
    }

    /**
     * Получить массив свободных мест на мероприятие
     * @return array массив из шлюза
     */
    public function getAvailPlaces()
    {
        $availPlaces = array();
        if ($this->client) {
            $availPlaces = $this->client->call('GetEvailPlaceList', array('NomBilKn' => $this->NomBilKn), true);
        }
        return $availPlaces;
    }

    /**
     * Получить массив зала мероприятия
     * @return array массив из шлюза
     */
    public function getHall()
    {
        /**
         * @todo с помощью метода GetHallList получить массив зала.
         * Вернуть этот массив.
         */
    }

    /**
     * Список мест сектора
     * @param string $codSec
     * @return array
     */
    public function getPlacesForSector($codSec)
    {
        $result = array();
        $places = $this->client->call('GetPlaceForSector', array(
            array(
                'NomBilKn' => $this->NomBilKn,
                'cod_sec' => $codSec
            )
        ), true);

        foreach ($places as $place) {
            $result[$place['row']][$place['seat']] = $place;
        }
        return $result;
    }

    /**
     * Информация об месте
     * @param string $codSec
     * @param string $row
     * @param string $seat
     * @return array
     */
    public function getPlaceInfo($codSec, $row, $seat)
    {
        if (!isset(self::$places[$codSec])) {
            self::$places[$codSec] = $this->getPlacesForSector($codSec);
        }
        return isset(self::$places[$codSec][$row][$seat]) ? self::$places[$codSec][$row][$seat] : array();
    }

    /**
     * Получить информацию о событии из базы
     * @param $eventId
     * @return mixed
     */
    public static function getMainInfo($eventId)
    {
        $data = db_select(self::EVENT_TABLE, 'e')
            ->fields('e')
            ->condition('e.event_id', $eventId)
            ->execute()
            ->fetchObject();

        return $data;
    }
}

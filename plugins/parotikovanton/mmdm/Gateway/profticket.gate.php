<?php

//класс логирования
require_once __DIR__ . '/profticket.log.php';

/**
 * Класс для работы с SOAP-сервисом ProfTicket
 */
class ProfTicketGate
{
    /**
     * @var SoapClient объект для работы с SOAP
     */
    private $_soap;
    /**
     * @var string пользователь для авторизации при SOAP запросах
     */
    private $_user;
    /**
     * @var string пароль для авторизации при SOAP запросах
     */
    private $_pass;
    /**
     * @var array последний ответ в виде массива
     */
    private $_lastAnswerResult;
    /**
     * @var string последний запрос
     */
    private $_lastRequest;
    /**
     * @var string последний ответ в виде XML
     */
    private $_lastResponse;

    /**
     * @var ProfTicketGate статичный объект класса
     */
    private static $_instance;

    /**
     * возвращает статичный объект класса
     * @return ProfTicketGate
     */
    public static function getInstance()
    {

        if (!self::$_instance) {
            self::$_instance = new self('http://195.9.173.102:8150/mmdmtest/ProfticketGate.exe/wsdl/IProfTicket', 'mmdmtest', '123456');
        }

        return self::$_instance;
    }

    /**
     * Конструктор создаёт объект класса SoapClient
     * @param $wsdl string Адрес WSDL
     * @param $user string Пользователь для авторизации
     * @param $pass string Пароль для авторизации
     */
    public function __construct($wsdl, $user, $pass)
    {
        $this->_user = $user;
        $this->_pass = $pass;
        try {
            $this->_soap = new SoapClient($wsdl);
        }
        catch (Exception $e) {
            ProfTicketLog::log('Error create SoapClient', null, ProfTicketLog::ERROR);
            $this->_soap = null;
        }
    }

    /**
     * Вызывает метод сервиса и возвращает нормализованный ответ
     * @param string $method имя метода
     * @param array $params массив массивов параметров
     * @param $notLog bool не логировать
     * @param $orderId int
     * @return array с ответом или false в случае ошибки
     */
    public function call($method, $params = array(), $notLog = false, $orderId = null)
    {
        //если клиент не создан - возвращаем пустой массив
        if (is_null($this->_soap)) {
            return array();
        }
        $ret = false;
        $paramsStr = $this->_getParams($params);

        $this->_lastRequest = $paramsStr;
        try {
            $res = $this->_soap->{$method}($paramsStr);
            $ret = $this->_extractResult($res);

        }
        catch (Exception $e) {
            $res = 'PHP ERROR NO ' . $e->getCode() . ': ' . $e->getMessage();
            ProfTicketLog::log($res, $method, ProfTicketLog::ERROR);
        }
        $this->_lastResponse = $res;
        if (!$notLog) {
            ProfTicketLog::log('Response: ' . $res, $method, ProfTicketLog::INFO, $orderId);
        }

        return $ret;
    }

    /**
     * Возвращает последний код ответа (AnswerResult) и его расшифровку в виде массива
     * @return mixed массив с кодом ответа и его расшифровку или false, если запроса еще не было
     */
    public function getLastAnswerResult()
    {
        return $this->_lastAnswerResult;
    }

    /**
     * Возвращает последний запрос в XML-формате
     * @param boolean $hideUserPass - заменить ли имя пользователя и пароль на звёздочки
     * @return string запрос
     */
    public function getLastRequest($hideUserPass = false)
    {
        $req = $this->_lastRequest;
        if ($hideUserPass) {
            $req = str_replace(
                array('<UserName>' . $this->_user . '</UserName>', '<UserPass>' . $this->_pass . '</UserPass>'),
                array('<UserName>***</UserName>', '<UserPass>***</UserPass>'),
                $req
            );
        }

        return $req;
    }

    /**
     * Получить последний ответ в XML формате
     * @return string ответ
     */
    public function getLastResponse()
    {
        return $this->_lastResponse;
    }

    /**
     * Формирование строки запроса для SOAP сервиса
     * @param array $params серока параметров
     * @return string
     */
    private function _getParams($params = array())
    {
        $reqBody =  '<?xml version="1.0" encoding="Windows-1251"?>';
        $reqBody .= '<GateReq>';
        $reqBody .=     '<ReqLogin>';
        $reqBody .=         '<UserName>' . $this->_user . '</UserName>';
        $reqBody .=         '<UserPass>' . $this->_pass . '</UserPass>';
        $reqBody .=     '</ReqLogin>';
        $reqBody .=     '<ReqBody>';
        foreach ($params as $keyParam => $param) {
            $reqBody .=         '<InputRow ';
            if (is_array($param)) {
                foreach ($param as $key => $val) {
                    $reqBody .= sprintf('%s="%s" ', $key, $val);
                }
            } else {
                $reqBody .= sprintf('%s="%s" ', $keyParam, $param);
            }
            $reqBody .=         '/>';
        }
        if (empty($params))  $reqBody .= '<InputRow/>';
        $reqBody .=     '</ReqBody>';
        $reqBody .= '</GateReq>';

        return $reqBody;
    }

    /**
     * Извлекает ответ сервера из xml ответа
     * @param string $xmlRes - ответ SOAP сервера
     * @return array Массив строк ответа
     */
    private function _extractResult($xmlRes)
    {
        $retArray = array();

        try {
            $obXml = new SimpleXMLElement($xmlRes);
            // тело ответа
            foreach ($obXml->AnswerBody->Row as $row) {
                $data = array();
                foreach ($row->attributes() as $k => $v) {
                    $data[$k] = (string) $v;
                }
                $retArray[] = $data;
            }
            // код ответа с расшифровкой
            if (!empty($obXml->AnswerResult)) {
                $this->_lastAnswerResult = (array) $obXml->AnswerResult;
            }
        }
        catch (Exception $e) {}

        return $retArray;
    }

    /**
     * Можно ли купить билеты заказа
     * @param ProfTicketOrder $order
     * @return bool
     * @throws Exception
     * @throws ProfTicketOrderException
     */
    public static function checkSold(ProfTicketOrder $order)
    {
        $ret = true;
        //проверка доступности битлетов для продажи
        $client = ProfTicketGate::getInstance();
        $params = self::clearParams($order->getTickets(), array('NomBilKn', 'cod_sec', 'row', 'seat', 'session', 'ReservID', 'price'));
        if (empty($params)) {
            throw new ProfTicketOrderException('Empty tickets params');
        }

        $result = $client->call('CheckSoldTickets', $params, false, $order->id);

        try {
            if (empty($result)) {
                throw new Exception('Empty response');
            }
            foreach ($result as $ticket) {
                if ($ticket['result_code'] != 0) {
                    throw new Exception('Error code');
                }
            }
        } catch (\Exception $e) {
            $ret = false;
        }
        return $ret;
    }

    /**
     * @param $arInput
     * @param $arParams
     * @return mixed
     */
    public static function clearParams($arInput, $arParams)
    {
        foreach ($arInput as $k => $val) {
            $arInput[$k] = array();
            foreach ($arParams as $paramKey) {
                $arInput[$k][$paramKey] = isset($val[$paramKey]) ? $val[$paramKey] : '';
            }
        }

        return $arInput;
    }
}

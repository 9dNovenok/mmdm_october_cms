<?php

class ProfTicketTools
{
    /**
     * Получение пользователей по названию роли
     * @param $roleName
     * @return array
     */
    public static function getUsersByRole($roleName)
    {
        $admins = db_select('users', 'u');
        $admins->join('users_roles', 'ur', 'u.uid = ur.uid');
        $admins->join('role', 'r', 'r.rid = ur.rid');
        $admins->fields('u', array('uid'))
            ->condition('r.name', $roleName, '=')
            ->condition('u.status', 1, '=');
        $result = $admins->execute();
        $usersData = $result->fetchCol();
        return !empty($usersData) ? $usersData : array();
    }

    /**
     * Получение значения поля списка
     * @param $field
     * @param $node
     * @return string
     */
    public static function getFieldValue($field, $node)
    {
        $eventType = field_get_items('node', $node, $field);
        $field = field_info_field($field);
        $values = $field['settings']['allowed_values'];
        return isset($values[$eventType[0]['value']]) ? $values[$eventType[0]['value']] : '';
    }

    /**
     * Получить IP текущего пользователя
     */
    public static function getRealIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }


    /**
     * возвращает список емейлов администраторов
     * @return array
     */
    public static function getAdminsEmails()
    {
        $result = array(
            PROFTICKET_ADMIN_EMAIL
        );
        $fromSettings = variable_get('profticket_admin_emails', '');
        if ($fromSettings) {
            $result = array();
            $emails = explode(',', $fromSettings);
            foreach ($emails as $email) {
                $result[] = trim($email);
            }
        }

        return $result;
    }

    /**
     * возвращает список емейлов уведомлений для брони
     * @return array
     */
    public static function getReserveEmails()
    {
        $result = array(
            PROFTICKET_ADMIN_EMAIL
        );
        $fromSettings = variable_get('profticket_reserve_emails', '');
        if ($fromSettings) {
            $result = array();
            $emails = explode(',', $fromSettings);
            foreach ($emails as $email) {
                $result[] = trim($email);
            }
        }

        return $result;
    }
}

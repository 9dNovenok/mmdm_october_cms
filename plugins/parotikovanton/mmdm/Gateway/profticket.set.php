<?php

require_once __DIR__ . '/profticket.gate.php';

/**
 * Класс для генерации исключений комплектов
 */
class ProfTicketSetException extends Exception
{

}

/**
 * Класс для работы с комплектами из шлюза.
 *
 * На вход конструктору передается идентификатор комплекта.
 */
class ProfTicketSet
{
    // todo может измениться
    const GET_LIST_METHOD_NAME = 'GetShowListEventsList';
    const SET_PRE_RESERVATION_METHOD = 'PreSetReservationEventsSet';
    const SET_RESERVATION_METHOD = 'SetReservationEventsSet';
    const FREE_PRE_RESERVATION_METHOD = 'FreePreReservationEventsSet';
    const FREE_RESERVATION_METHOD = 'FreeReservationEventsSet';
    const SET_SOLD_METHOD = 'SetSoldEventsSet';

    /**
     * @var boolean Включить кеш или нет (удалить этот флаг, когда можно будет реально работать с кешем)
     */
    protected $cacheEnabled = false;

    /**
     * @var array массив полей события из шлюза
     */
    protected $_fields = array();

    /**
     * @var клиент
     */
    protected $client;

    /**
     * @var array места с ценами по секторам и рядам
     */
    public static $places;

    /**
     * Конструктор. На вход передается идентификатор комплекта в шлюзе.
     *
     * @param int $setId
     * @param bool $force
     * @param string $langCode
     * @throws ProfTicketEventException
     */
    public function __construct($setId, $force = false, $langCode = 'ru')
    {
        $this->client = ProfTicketGate::getInstance();

        //получаем параметры из шлюза
        $set = $this->getSetById($setId);
        if (!empty($set)) {
            $this->_fields = $set;
        }

        if (empty($this->_fields)) {
            throw new ProfTicketSetException('Empty fields');
        }

        $this->_fields['node'] = self::getSetNode($setId, $langCode);

        if (empty($this->_fields['node'])) {
            throw new ProfTicketEventException('Node not found');
        }

    }

    /**
     * Получить ноду комплекта
     * @param int $setId
     * @param string $langCode
     * @return array|null
     */
    public static function getSetNode($setId, $langCode = 'ru')
    {
        $node = null;
        //получаем параметры из базы
        $query = new EntityFieldQuery();
        $resultQuery = $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'set')
            ->fieldCondition('field_set_id', 'value', $setId)
            ->propertyCondition('language', $langCode, '=')
            ->execute();


        if (!empty($resultQuery['node'])) {
            $result = $resultQuery;
        }

        if (!empty($result['node'])) {
            $nodeIds = array_keys($result['node']);
            if (count($nodeIds) == 1) {
                $node = node_load($nodeIds[0]);
            }
        }

        return $node;
    }

    /**
     * Получить данные комплекта из шлюза
     *
     * @param integer $setId Идентификатор
     * @param boolean $force Попытка получить мероприятие без мест (false по умолчанию)
     * @return array поля мероприятия из шлюза
     */
    protected function getSetById($setId, $force = false)
    {
        if ($this->cacheEnabled) {
            //получаем параметры мероприятия из шлюза
            $cacheId = $force ? 'GetSetByIdForce_' . $setId : 'GetSetById_' . $setId;
            $cacheBin = 'cache';
            $cacheTime = time() + (60 * 15); // на 15 минут
            $cache = cache_get($cacheId, $cacheBin);
            if (isset($cache->data) && isset($cache->expire) && $cache->expire > time()) {
                return $cache->data;
            }
            cache_clear_all($cacheId, $cacheBin);
        }
        $query = array(
            'EventsSetID' => $setId,
        );

        $res = $this->client->call(self::GET_LIST_METHOD_NAME, $query, true);
        $cachedEvent = array();

        foreach ($res as $data) {
            $cachedEvent['EventsSetID'] = $data['EventsSetID'];
            $cachedEvent['FreePlacesSet'] = $data['FreePlacesSet'];
            $cachedEvent['CountEvents'] = $data['CountEvents'];
            $cachedEvent['EventsSetName'] = $data['EventsSetName'];
            $cachedEvent['ETicketPermitted'] = $data['ETicketPermitted'];
            $cachedEvent['cod_h'] = $data['cod_h'];
            $cachedEvent['cod_t'] = $data['cod_t'];
            break;
        }
        if ($this->cacheEnabled) {
            cache_set($cacheId, $cachedEvent, $cacheBin, $cacheTime);
        }
        return $cachedEvent;
    }

    /**
     * Получить поле события
     * @param string $fieldName
     * @return
     */
    public function __get($fieldName)
    {
        return isset($this->_fields[$fieldName]) ? $this->_fields[$fieldName] : null;
    }

    /**
     * Получить схему зала
     * @return array массив объектов зала из шлюза
     */
    public function getHallScheme()
    {
        $places = array();
        if ($this->client) {
            $places = $this->client->call('GetSchemaHallListEventsSet', array('EventsSetID' => $this->EventsSetID), true);
        }

        return $places;
    }

    /**
     * Получить массив свободных мест
     * @return array массив из шлюза
     */
    public function getAvailPlaces()
    {
        $availPlaces = array();
        if ($this->client) {
            $availPlaces = $this->client->call('GetEvailPlaceListEventsSet', array('EventsSetID' => $this->EventsSetID), true);
        }
        return $availPlaces;
    }
}

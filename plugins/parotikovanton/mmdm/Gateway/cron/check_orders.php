<?php
/**
 * Проверка статусов мероприятий
 * запуск раз в пол часа
 */
require_once __DIR__ . '/head.php';

/**
 * Нас интересуют только заказы со статусом NEW, STATUS_ERROR_SET_SOLD или PENDING
 * если с момента создания прошло более часа, а заказ все еще в этом статусе - необходимо выполнить отмену
 *
 * если заказ в статусе STATUS_ERROR_SET_SOLD и час не прошел - пытается проставить оплату
 */

$or = db_or();
$or->condition('payment_status_id', ProfTicketOrder::STATUS_NEW);
$or->condition('payment_status_id', ProfTicketOrder::STATUS_PENDING);
$or->condition('payment_status_id', ProfTicketOrder::STATUS_ERROR_SET_SOLD);
$or->condition('payment_status_id', ProfTicketOrder::STATUS_ERROR_SET_RETURN);
$or->condition('payment_status_id', ProfTicketOrder::STATUS_PAYMENT_ERROR);

$and = db_and();
$and->condition('created', date('Y-m-d H:i:s', time() - 60 * 60 * 24), '>');
$and->condition($or);

$orders = db_select('profticket_order', 'o')->fields('o', array(
    'id',
    'created',
    'payment_status_id',
))->condition($and)->execute();

foreach ($orders as $order) {
    $order = ProfTicketOrder::getById($order->id);

    if (!($order instanceof ProfTicketOrder)) {
        continue;
    }

    print "order #{$order->id} ";

    try {
        ProfTicketPaymentManager::processOrder($order);
    }  catch (Exception $e) {}

    if (strtotime($order->created) + 60 * 60 > time()) {
        print "not expired\n";
        continue;
    }

    print "expired\n";


    if ($order->payment_status_id == ProfTicketOrder::STATUS_NEW
        || $order->payment_status_id == ProfTicketOrder::STATUS_PENDING
        || $order->payment_status_id == ProfTicketOrder::STATUS_ERROR_SET_SOLD
        || $order->payment_status_id == ProfTicketOrder::STATUS_ERROR_SET_RETURN
        || $order->payment_status_id == ProfTicketOrder::STATUS_PAYMENT_ERROR
    ) {
        ProfTicketLog::log('Автоматическая отмена заказа №' . $order->id, null, ProfTicketLog::WARNING, $order->id);
        $order->rollBack();

        if ($order->payment_status_id != ProfTicketOrder::STATUS_ROLLBACK) {
            continue;
        }

        // отправляем письмо пользователю
        try {
            $mail = profticket_get_mail_client();
            $mail->addAddress($order->user_email);
            $mail->isHTML(true);
            $mail->Subject = 'Заказ №' . $order->id . ' не оплачен';

            $mail->Body = 'Здравствуйте, ' . $order->name . '!<br />
                К сожалению, ваш заказ №' . $order->id . ' отменен, так как не был оплачен.';

            $mail->send();
        } catch (\Exception $e) {}
    }

}






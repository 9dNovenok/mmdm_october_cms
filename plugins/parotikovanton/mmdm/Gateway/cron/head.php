<?php
/**
 * Заголовочный файл для крон-скриптов
 */
// подключить Drupal
define('DRUPAL_ROOT', realpath(__DIR__ . '/../../../../../'));

$_SERVER['DOCUMENT_ROOT'] = DRUPAL_ROOT;
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

chdir(DRUPAL_ROOT);

require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

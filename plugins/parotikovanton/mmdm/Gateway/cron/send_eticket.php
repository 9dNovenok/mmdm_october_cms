<?php
/**
 * Создание и рассылка электронных билетов по заказу
 *
 * запуск раз в минуту
 */

require_once __DIR__ . '/head.php';

//Проверяем, заблокирован ли скрипт рассылки
$lock = false;
$lockFile = $lockFile = ProfTicketOrder::tmpDir() . '/eticket_send_script';
if (is_file($lockFile) && ((int) file_get_contents($lockFile)) > time() - 60 * 10) {
    $lock = true;
}

if ($lock) {
    fwrite(STDOUT, 'Скрипт заблокирован' . PHP_EOL);
    die();
}

//блокируем скрипт
file_put_contents($lockFile, time());

$andCondition = db_and();
$andCondition->condition('eticket_sent', '0');
$andCondition->condition('payment_status_id', ProfTicketOrder::STATUS_PAID);

$orders = db_select('profticket_order', 'o')->fields('o', array(
    'id',
))->condition($andCondition)->execute();

foreach ($orders as $order) {
    $order = ProfTicketOrder::getById($order->id);

    if (!($order instanceof ProfTicketOrder)) {
        continue;
    }

    fwrite(STDOUT, 'Отправка билетов по заказу ' . $order->id . PHP_EOL);

    //Отправляем билеты
    try {
        $order->sendETickets();
    } catch (Exception $ex) {
        fwrite(STDOUT, 'Ошибка отправки билетов по заказу ' . $order->id . PHP_EOL);
        continue;
    }

    fwrite(STDOUT, 'Билеты отправлены по заказу ' . $order->id . PHP_EOL);
}

//Удаляем lock
if (is_file($lockFile)) {
    unlink($lockFile);
}

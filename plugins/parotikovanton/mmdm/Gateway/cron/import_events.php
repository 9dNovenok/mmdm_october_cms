<?php
/**
 * Импорт мероприятий
 * запуск раз в час
 */

require_once __DIR__ . '/head.php';

$client = ProfTicketGate::getInstance();
// основной язык материалов
$mainLangCode = 'ru';

$soapItems = array();
$soapEventsAll = array();

// список сцен в справочнике сайта
$hallListOriginal = array();
$fieldHall = field_read_field('field_hall_full');
if (isset($fieldHall['settings']['allowed_values']) && !empty($fieldHall['settings']['allowed_values'])) {
    $hallListOriginal = $fieldHall['settings']['allowed_values'];
} else {
    ProfTicketLog::log('Ошибка импорта событий: Не удалось получить список залов', null, ProfTicketLog::ERROR);
}
$hallList = array_flip($hallListOriginal);

// список возрастных цензов
$ageListOriginal = array();
$fieldAge = field_read_field('field_age_rating');
if (isset($fieldAge['settings']['allowed_values']) && !empty($fieldAge['settings']['allowed_values'])) {
    $ageListOriginal = $fieldAge['settings']['allowed_values'];
} else {
    ProfTicketLog::log('Ошибка импорта событий: Не удалось получить список возрастных цензов', null, ProfTicketLog::ERROR);
}
$ageList = array_flip($ageListOriginal);

// список категорий мероприятия
$typeListOriginal = array();
$fieldType = field_read_field('field_type_of_event');
if (isset($fieldType['settings']['allowed_values']) && !empty($fieldType['settings']['allowed_values'])) {
    $typeListOriginal = $fieldType['settings']['allowed_values'];
} else {
    ProfTicketLog::log('Ошибка импорта событий: Не удалось получить список типов события', null, ProfTicketLog::ERROR);
}
$typeList = array_flip($typeListOriginal);

// список жанров
$genresListId = null;
$genresList = array();
$vocabularies = taxonomy_vocabulary_get_names();
if (isset($vocabularies['janer'])) {
    $genresListId = $vocabularies['janer']->vid;
    $genresList = taxonomy_get_tree($genresListId);
}
$genresListByName = array();
if (!empty($genresList)) {
    foreach ($genresList as $genre) {
        $genresListByName[$genre->name] = $genre->tid;
    }
} else {
    ProfTicketLog::log('Ошибка импорта событий: Не удалось получить список жанров', null, ProfTicketLog::ERROR);
}

// список залов театра по коду
$hallsByCode = array();

/**
 * Название зала по коду
 * @param ProfTicketGate $client
 * @param int $codeT
 * @param int $codeHall
 * @return null|string
 */
function getHallNameByCode(ProfTicketGate $client, $codeT, $codeHall)
{
    $hallName = null;
    $halls = $client->call('GetHallList', array('cod_t' => $codeT));
    if (!empty($halls)) {
        foreach ($halls as $hallInfo) {
            if ($hallInfo['cod_th'] == $codeHall) {
                $hallName = $hallInfo['name_h'];
                break;
            }
        }
    }
    return $hallName;
}

//получим id администратора
$adminId = 0;
$admins = ProfTicketTools::getUsersByRole('administrator');
if (!empty($admins)) {
    $adminId = $admins[0];
}


// получение событий
$soapEvents = $client->call('GetEventList', array(), true);
dd($soapEvents);

/**
 * Заносим данные о текущих событиях в бд
 */
// помечаем все события как необработанные
db_update(ProfTicketEvent::EVENT_TABLE)->fields(array(
    'updated' => 0,
))->execute();

// обновляем данные событий
if (!empty($soapEvents)) {
    foreach ($soapEvents as $event) {
        $eventTime = DateTime::createFromFormat('d.m.Y H:i:s', $event['EventDate'] . ' ' . $event['EventTime']);
        $sql = "REPLACE INTO " . ProfTicketEvent::EVENT_TABLE . "
         SET
         event_id = :event_id,
         `date` = :event_date,
         free_places_count = :free_places_count,
         e_ticket_permitted =:e_ticket_permitted,
         updated = 1
        ";
        db_query($sql, array(
            ':event_id' => (int)$event['NomBilKn'],
            ':event_date' => $eventTime->format('Y-m-d H:i:s'),
            ':free_places_count' => (int) $event['FreePlacesQty'],
            ':e_ticket_permitted' => (int) $event['ETicketPermitted']
        ));
    }
}

// удаляем необновленные записи
db_delete(ProfTicketEvent::EVENT_TABLE)->condition('updated', 0)->execute();


// получение событий без мест
$soapSubEvents = $client->call('GetEventList', array('FreeOnly' => 0), true);

//все вместе
foreach ($soapEvents as $event) {
    $soapEventsAll[] = $event;
}

foreach ($soapSubEvents as $event) {
    $soapEventsAll[] = $event;
}

// соотвествие id события в шлюзе к nodeId
$eventByExternalId = array();

/** @var array $seasonEvents события-абонементы */
$seasonEvents = array();
$seasonSubEvents = array();

foreach ($soapEventsAll as $event) {
    //дата в базе должна храниться в UTC
    $eventTime = DateTime::createFromFormat('d.m.Y H:i:s', $event['EventDate'] . ' ' . $event['EventTime']);
    if (!$eventTime) {
        $dateEvent = array();
    }
    else {
        $eventTime->setTimezone(new DateTimeZone('UTC'));
        $dateEvent = array(
            'value' => $eventTime->format('Y-m-d H:i:s'),
        );
    }

    if ($event['SeasonTicket'] == 1) {
        /**
         * событие-абонемент
         */
        $seasonEvents[] = $event;
        continue;

    }

    //если события с таким номером нет - заносим в базу
    $nodeId = 0;
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'events')
        ->fieldCondition('event_external_id', 'value', $event['NomBilKn'])
        ->execute();


    if (empty($result['node'])) {
        try {
            $node = new stdClass();
            $node->type = 'events';
            node_object_prepare($node);
            $node->title = $event['name_show'];
            $node->language = $mainLangCode;
            $node->event_external_id[LANGUAGE_NONE][0]['value'] = $event['NomBilKn'];
            $node->field_data_event[LANGUAGE_NONE][0] = $dateEvent;

            // синхранизируем дополнительные поля события
            // зал
            $hallName = isset($event['name_h']) ? $event['name_h'] : null;
            $hallId = isset($hallList[$hallName]) ? $hallList[$hallName] : null;
            if (!$hallId) {
                ProfTicketLog::log('Ошибка импорта событий: Зал "' . $hallName . '" не найден на сайте', null, ProfTicketLog::ERROR);
            } else {
                // заносим зал в ноду
                $node->field_hall_full[LANGUAGE_NONE][0]['value'] = $hallId;
            }

            // возрастное ограничение
            $age = isset($event['Age']) && $event['Age'] >= 0 ?  $event['Age'] . '+' : null;
            if ($age) {
                $ageValueId = isset($ageList[$age]) ? $ageList[$age] : null;
                if (!$ageValueId) {
                    ProfTicketLog::log('Ошибка импорта событий: Ценз "' . $age . '" не найден на сайте', null, ProfTicketLog::ERROR);
                } else {
                    // заносим ценз в ноду
                    $node->field_age_rating[LANGUAGE_NONE][0]['value'] = $ageValueId;
                }
            }

            // продолжительность
            $duration = isset($event['EventDuration']) && $event['EventDuration'] ?  $event['EventDuration'] : null;
            if ($duration) {
                $node->field_duration[LANGUAGE_NONE][0]['value'] = $duration;
            }

            // данные организатора
            if ($event['OrganizerID']) {
                $node->field_organizer_id[LANGUAGE_NONE][0]['value'] = $event['OrganizerID'];
            }
            if ($event['OrganizerName']) {
                $node->field_organizer_name[LANGUAGE_NONE][0]['value'] = $event['OrganizerName'];
            }
            if ($event['OrganizerTin']) {
                $node->field_organizer_tin[LANGUAGE_NONE][0]['value'] = $event['OrganizerTin'];
            }
            if ($event['OrganizerAddress']) {
                $node->field_organizer_address[LANGUAGE_NONE][0]['value'] = $event['OrganizerAddress'];
            }

            // категория
            $categories = $client->call('GetShowCategoriesList', array(
                'code_show' => $event['cod_show'],
            ));
            if (!empty($categories)) {
                $category = reset($categories);
                $categoryName = $category['name_categ'];
                if ($categoryName) {
                    $typeValueId = isset($typeList[$categoryName]) ? $typeList[$categoryName] : null;
                    if (!$typeValueId) {
                        ProfTicketLog::log('Ошибка импорта событий: Категория "' . $categoryName . '" не найдена на сайте', null, ProfTicketLog::ERROR);
                    } else {
                        // заносим ноду
                        $node->field_type_of_event[LANGUAGE_NONE][0]['value'] = $typeValueId;
                    }
                }
            }

            // жанры
            $ganderForSave = array();
            $genres = $client->call('GetShowGenresList', array(
                'code_show' => $event['cod_show'],
            ));

            if (!empty($genres) && !empty($genresListByName)) {
                foreach ($genres as $genre) {
                    $genreName = $genre['name_genre'];

                    // если жанра нет в таксономии - добавляем
                    if (!isset($genresListByName[$genreName])) {
                        $term = (object) array(
                            'vid' => $genresListId,
                            'name' => $genreName,
                        );
                        if (taxonomy_term_save($term)) {
                            $genresListByName[$genreName] = $term->tid;
                        }
                    }

                    $tid = isset($genresListByName[$genreName]) ? $genresListByName[$genreName] : null;
                    if ($tid) {
                        $ganderForSave[] = $tid;
                    }
                }
            }

            if (!empty($ganderForSave)) {
                foreach ($ganderForSave as $key => $genderTid) {
                    $node->field_tags[LANGUAGE_NONE][$key]['tid'] = $genderTid;
                }
            }

            //не публикуем
            $node->status = 0;
            $node->uid = $adminId;
            node_save($node);

            $nodeId = $node->nid;

            //проверяем сгенерированный alias на уникальность
            $path = path_load(array('source' => 'node/' . $node->nid));
            $existsPaths = path_load(array('alias' => $path['alias']));
            if (!empty($existsPaths) && $existsPaths['source'] != 'node/' . $node->nid) {
                $path['alias'] = $path['alias'] . '-' . $event['NomBilKn'];
                path_save($path);
            }

            ProfTicketLog::log('Create event #' . $event['NomBilKn']);
        } catch (Exception $e) {
            ProfTicketLog::log('Error create event #' . $event['NomBilKn'], null, ProfTicketLog::ERROR);
        }
    } else {
        //обновляем дату если изменилась, у всех материалов, независимо от языка
        try {
            $nodeIds = array_keys($result['node']);
            if (empty($nodeIds)) {
                ProfTicketLog::log('Error update event #' . $event['NomBilKn'] . ' - empty event ids', null, ProfTicketLog::WARNING);
                continue;
            }

            foreach ($nodeIds as $id) {
                $node = node_load($id);
                if ($node->language == $mainLangCode) {
                    // обновляем название у события основного языка
                    $node->title = $event['name_show'];
                    $nodeId = $node->nid;
                }

                // продолжительность
                $node->field_duration[LANGUAGE_NONE][0]['value'] = isset($event['EventDuration']) ?  $event['EventDuration'] : null;

                // возрастное ограничение
                $age = isset($event['Age']) && $event['Age'] >= 0 ?  $event['Age'] . '+' : null;
                if ($age) {
                    $ageValueId = isset($ageList[$age]) ? $ageList[$age] : null;
                    if (!$ageValueId) {
                        ProfTicketLog::log('Ошибка импорта событий: Ценз "' . $age . '" не найден на сайте', null, ProfTicketLog::ERROR);
                    } else {
                        // заносим ценз в ноду
                        $node->field_age_rating[LANGUAGE_NONE][0]['value'] = $ageValueId;
                    }
                }

                if ($node->field_data_event[LANGUAGE_NONE][0]['value'] != $dateEvent['value']) {
                    $node->field_data_event[LANGUAGE_NONE][0] = $dateEvent;
                }

                node_save($node);
            }

        } catch (Exception $e) {
            ProfTicketLog::log('Error update event #' . $event['NomBilKn'], null, ProfTicketLog::ERROR);
        }
    }

    /**
     *  событие  прикрепленное к абонементу
     */
    if ($event['SeasonTicket'] == 2 && $nodeId) {
        if (!isset($seasonSubEvents[$event['SeasonTicketId']])) {
            $seasonSubEvents[$event['SeasonTicketId']] = array();
        }
        $seasonSubEvents[$event['SeasonTicketId']][] = array('nid' => $nodeId);
    }

    if ($nodeId) {
        $eventByExternalId[$event['NomBilKn']] = $nodeId;
    }
}


/**
 *  Импортируем абонементы в самом конце, чтобы прикрепить мероприятия
 */
foreach ($seasonEvents as $event) {
    //дата в базе должна храниться в UTC
    $eventTime = DateTime::createFromFormat('d.m.Y H:i:s', $event['EventDate'] . ' ' . $event['EventTime']);
    $eventTime->setTimezone(new DateTimeZone('UTC'));
    //год абонемента
    $eventYear = $eventTime->format('Y-01-01 00:00:00');

    //прикрепленные мероприятия
    $subEvents = isset($seasonSubEvents[$event['SeasonTicketId']]) ? $seasonSubEvents[$event['SeasonTicketId']] : null;

    $nodeId = null;

    //если событие-абонемент с таким номером нет - заносим в базу, интересуют только материалы на русском
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'season_ticket')
        ->fieldCondition('field_season_ticket_id_1', 'value', $event['NomBilKn'])
        ->propertyCondition('language', $mainLangCode, '=')
        ->execute();

    if (empty($result['node'])) {
        try {
            $node = new stdClass();
            $node->type = 'season_ticket';
            node_object_prepare($node);
            $node->title = $event['name_show'];
            $node->language = $mainLangCode;
            $node->field_season_ticket_id_1[LANGUAGE_NONE][0]['value'] = $event['NomBilKn'];
            $node->field_god_abonimenta[LANGUAGE_NONE][0]['value'] = $eventYear;

            //прикрепляем мероприятия
            $node->field_season_ticket_id[LANGUAGE_NONE] = $subEvents;

            //не публикуем
            $node->status = 0;
            $node->uid = $adminId;
            node_save($node);

            $nodeId = $node->nid;

            //проверяем сгенерированный alias на уникальность
            $path = path_load(array('source' => 'node/' . $node->nid));
            $existsPaths = path_load(array('alias' => $path['alias']));
            if (!empty($existsPaths) && $existsPaths['source'] != 'node/' . $node->nid) {
                $path['alias'] = $path['alias'] . '-' . $event['NomBilKn'];
                path_save($path);
            }

            ProfTicketLog::log('Create season event #' . $event['NomBilKn']);
        } catch (Exception $e) {
            ProfTicketLog::log('Error create season event #' . $event['NomBilKn'], null, ProfTicketLog::ERROR);
        }
    } else {
        //обновляем дату если изменилась
        try {
            $nodeIds = array_keys($result['node']);
            if (count($nodeIds) > 1) {
                ProfTicketLog::log('Error update event #' . $event['NomBilKn'] . ' - duplicate russian season event_external_id', null, ProfTicketLog::WARNING);
                continue;
            }
            $node = node_load($nodeIds[0]);

            $nodeId = $node->nid;

            if ($node->field_god_abonimenta[LANGUAGE_NONE][0]['value'] != $eventYear || count($node->field_season_ticket_id[LANGUAGE_NONE]) != count($subEvents)) {
                //прикрепляем мероприятия
                $node->field_season_ticket_id[LANGUAGE_NONE] = $subEvents;
                //год
               $node->field_god_abonimenta[LANGUAGE_NONE][0]['value'] = $eventYear;
                node_save($node);
                ProfTicketLog::log('Update season event #' . $event['NomBilKn'], null, ProfTicketLog::ERROR);
            }


        } catch (Exception $e) {
            ProfTicketLog::log('Error update event #' . $event['NomBilKn'], null, ProfTicketLog::ERROR);
        }
    }

    if ($nodeId) {
        $eventByExternalId[$event['NomBilKn']] = $nodeId;
    }
}

/**
 * Импорт комплектов
 */

// принадлежность событий к комплектам
$showSetsEvents = array();

// получаем связи комплектов с событиями
$soapSetsEventsList = $client->call(ProfTicketSet::GET_LIST_METHOD_NAME, array(), true);
foreach ($soapSetsEventsList as $soapSetsEvents) {
    $setId = $soapSetsEvents['EventsSetID'];
    $eventId = $soapSetsEvents['NomBilKn'];

    $eventTimeKey = time() . uniqid();
    try {
        $eventDateTime = DateTime::createFromFormat('d.m.Y H:i:s', $soapSetsEvents['EventDate'] . ' ' . $soapSetsEvents['EventTime']);
        $eventTimeKey = $eventDateTime->getTimestamp() . uniqid();
    } catch (\Exception $e) {}

    if (!isset($showSetsEvents[$setId])) {
        $showSetsEvents[$setId] = array();
    }
    $nodeEventId = isset($eventByExternalId[$eventId]) ? $eventByExternalId[$eventId] : null;
    if ($eventId && $setId) {
        $showSetsEvents[$setId][$eventTimeKey] = array('nid' => $nodeEventId);
    }
}


// синхранизация самих комплектов
$soapEventSets = $client->call(ProfTicketSet::GET_LIST_METHOD_NAME, array('ListOnly' => 1), true);

foreach ($soapEventSets as $soapEventSet) {
    // создаем или обновляем комплект
    //если комплект с таким номером нет - заносим в базу, интересуют только материалы на русском
    $setId = $soapEventSet['EventsSetID'];

    // прикрепленные события
    $subEventsNotSorted = isset($showSetsEvents[$setId]) ? $showSetsEvents[$setId] : null;

    $subEvents = null;
    if (!empty($subEventsNotSorted)) {
        $subEvents = array();
        ksort($subEventsNotSorted);
        reset($subEventsNotSorted);
        // сбрасываем ключи
        foreach ($subEventsNotSorted as $subEvent) {
            $subEvents[] = $subEvent;
        }
    }

    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'set')
        ->fieldCondition('field_set_id', 'value', $setId)
        ->propertyCondition('language', $mainLangCode, '=')
        ->execute();

    if (empty($result['node'])) {
        try {
            $node = new stdClass();
            $node->type = 'set';
            node_object_prepare($node);
            $node->title = $soapEventSet['EventsSetName'];
            $node->language = $mainLangCode;
            $node->field_set_id[LANGUAGE_NONE][0]['value'] = $setId;
            $node->field_event_counts[LANGUAGE_NONE][0]['value'] = $soapEventSet['CountEvents'];

            // зал
            $hallCode = isset($soapEventSet['cod_h']) ? $soapEventSet['cod_h'] : null;
            $hallName = getHallNameByCode($client, $soapEventSet['cod_t'], $hallCode);
            $hallId = isset($hallList[$hallName]) ? $hallList[$hallName] : null;
            if (!$hallId) {
                ProfTicketLog::log('Ошибка импорта комплектов: Зал "' . $hallName . ' (' . $hallCode . ')" не найден на сайте', null, ProfTicketLog::ERROR);
            } else {
                // заносим зал в ноду
                $node->field_hall_full[LANGUAGE_NONE][0]['value'] = $hallId;
            }

            //прикрепляем события
            $node->field_events[LANGUAGE_NONE] = $subEvents;

            //не публикуем
            $node->status = 0;
            $node->uid = $adminId;
            node_save($node);

            //проверяем сгенерированный alias на уникальность
            $path = path_load(array('source' => 'node/' . $node->nid));
            $existsPaths = path_load(array('alias' => $path['alias']));
            if (!empty($existsPaths) && $existsPaths['source'] != 'node/' . $node->nid) {
                $path['alias'] = $path['alias'] . '-' . $setId;
                path_save($path);
            }

            ProfTicketLog::log('Create events set #' . $setId);
        } catch (Exception $e) {
            ProfTicketLog::log('Error events set #' . $setId, null, ProfTicketLog::ERROR);
        }
    } else {
        //обновляем поля
        try {
            $nodeIds = array_keys($result['node']);
            if (count($nodeIds) > 1) {
                ProfTicketLog::log('Error update events set #' . $setId . ' - duplicate russian set_id', null, ProfTicketLog::WARNING);
                continue;
            }
            $node = node_load($nodeIds[0]);

            $node->title = $soapEventSet['EventsSetName'];
            $node->field_set_id[LANGUAGE_NONE][0]['value'] = $setId;
            $node->field_event_counts[LANGUAGE_NONE][0]['value'] = $soapEventSet['CountEvents'];

            // зал
            // todo временно отключаем
            /**
            $hallCode = isset($soapEventSet['cod_h']) ? $soapEventSet['cod_h'] : null;
            $hallName = getHallNameByCode($client, $soapEventSet['cod_t'], $hallCode);
            $hallId = isset($hallList[$hallName]) ? $hallList[$hallName] : null;
            if (!$hallId) {
                ProfTicketLog::log('Ошибка импорта комплектов: Зал "' . $hallName . ' (' . $hallCode . ')" не найден на сайте', null, ProfTicketLog::ERROR);
            } else {
                // заносим зал в ноду
                $node->field_hall_full[LANGUAGE_NONE][0]['value'] = $hallId;
            }
             **/

            //прикрепляем события
            $node->field_events[LANGUAGE_NONE] = $subEvents;

            node_save($node);
            ProfTicketLog::log('Update events set #' . $setId, null, ProfTicketLog::ERROR);
        } catch (Exception $e) {
            ProfTicketLog::log('Error events set #' . $setId, null, ProfTicketLog::ERROR);
        }
    }
}

<?php
/**
 * удаление старых логов
 * запуск раз в сутки
 */
require_once __DIR__ . '/head.php';

/**
 * Удаляем старше 3х месяцев
 */

$dateTime = new DateTime();
$interval = new DateInterval('P3M');
$dateTime->sub($interval);

$dateTo = $dateTime->format('Y-m-d H:i:s');
$logs = db_delete('profticket_log')->condition('date', $dateTo, '<=')->execute();

print 'Clear before ' . $dateTo;





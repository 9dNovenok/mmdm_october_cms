<?php

require_once __DIR__ . '/profticket.gate.php';
require_once __DIR__ . '/profticket.mpdf.php';
require_once __DIR__ . '/profticket.event.php';

// qr code
include(__DIR__ .  '../../libraries/phpqrcode/qrlib.php');

/**
 * Класс для генерации исключений заказов
 */
class ProfTicketOrderException extends Exception
{

}

/**
 * Класс для работы с заказами билетов.
 *
 * Создание заказа производится с помощью метода ProfTicketOrder::createOrder.
 * Создание резерва производится с помощью метода ProfTicketOrder::createReservation.
 * Получение заказа по идентификатору производитя с помощью метода ProfTicketOrder::getById.
 * Оплата заказа производится с помощью метода ProfTicketOrder::pay.
 * Откат оплаты производится с помощью метода ProfTicketOrder::rollBack.
 * Пометить заказ оплаченным можно с помощью метода ProfTicketOrder::setSold.
 *
 * Сохранение (update и insert) заказа производится в методе save().
 *
 * @property string $payment_system_order_id
 * @property int $payment_status_id
 * @property string $payment_system_code
 * @property int $id
 * @property string $payment_transaction_id
 * @property string $user_email
 * @property float $tickets_sum
 * @property string $phone
 * @property string $name
 * @property string $cancel_date
 * @property string $payment_date
 *
 * @property int|null $user_id id пользователя, сделавшего заказ
 * @property int $eticket_sent Флаг отправки электронных билетов (0|1)
 */
class ProfTicketOrder
{
    /**
     * Статус заказа - новый
     */
    const STATUS_NEW = 0;

    /**
     * Статус заказа - бронирование
     */
    const STATUS_RESERVATION = 1;

    /**
     * Статус заказа - оплачен
     */
    const STATUS_PAID = 2;

    /**
     * Статус заказа - ошибка оплаты
     */
    const STATUS_PAYMENT_ERROR = 3;

    /**
     * Статус заказа - возврат денег
     */
    const STATUS_ROLLBACK = 4;

    /**
     * Статус заказа - ручная обработка
     */
    const STATUS_PENDING = 5;

    /**
     * Статус заказа - ошибка отмены
     */
    const STATUS_ERROR_ROLLBACK = 6;

    /**
     * Статус - ошибка установки продажи
     */
    const STATUS_ERROR_SET_SOLD = 7;

    /**
     * Ошибка возврата билета в шлюз
     */
    const STATUS_ERROR_SET_RETURN = 8;

    /**
     * Ошибка возврата билета в шлюз, окончательная
     */
    const STATUS_ERROR_SET_RETURN_END = 9;

    /**
     * @var array поля заказа в БД
     */
    protected $_fields;

    /**
     * @var array массив билетов
     */
    protected $_tickets;

    /**
     * @var array
     */
    public $events = array();

    /**
     * Конструктор заказа. На вход передается массив полей и массив билетов.
     *
     * @param array $fields поля заказа, включая id
     * @param array $tickets билеты заказа, включая id
     */
    protected function __construct($fields, $tickets)
    {
        $this->_fields = $fields;
        $this->_tickets = $tickets;
    }

    /**
     * Получить массив билетов
     * @return array
     */
    public function getTickets()
    {
        return $this->_tickets;
    }

    /**
     * Получить поле заказа
     * @param string $fieldName название поля
     * @return string|int|null
     */
    public function __get($fieldName)
    {
        return isset($this->_fields[$fieldName]) ? $this->_fields[$fieldName] : null;
    }

    /**
     * Магический метод для установки виртуального свойства
     *
     * @param string $name Название виртуального свойства
     * @param mixed $value Значение виртуального свойства для установки
     *
     * @return void
     *
     * @throws Exception
     */
    public function __set($name, $value)
    {
        $this->_fields[$name] = $value;
    }

    /**
     * Поиск заказа по идентификатору.
     * В случае успеха возвращает объект заказа, в случае ошибки - null.
     *
     * @param int $orderId
     * @return self|null
     */
    public static function getById($orderId)
    {
        $ret = null;

//        $result = db_select('profticket_order', 'o')->fields('o', array(
//            'id',
//            'tickets_sum',
//            'created',
//            'reservation_id',
//            'payment_status_id',
//            'user_email',
//            'payment_system_order_id',
//            'payment_transaction_id',
//            'payment_date',
//            'name',
//            'phone',
//            'payment_system_code',
//            'user_id',
//            'cancel_date',
//            'eticket_sent',
//        ))->condition('o.id', $orderId)->execute()->fetchObject();
        $result = [
            'id'=>1,
            'tickets_sum'=>1500,
            'created'=>'admin',
            'reservation_id'=>5,
            'payment_status_id'=>2,
            'user_email'=>'test@test.test',
            'payment_system_order_id'=>6100,
            'payment_transaction_id'=>4588,
            'payment_date'=>'17-05-2015',
            'name'=>'name',
            'phone'=>89109054040,
            'payment_system_code'=>8,
            'user_id'=>199,
            'cancel_date'=>null,
            'eticket_sent'=>'hz'
        ];
        if (!empty($result)) {
            $tickets = array();
//            $ticketsResult = db_select('profticket_ordered_tickets', 't')->fields('t', array(
//                'id',
//                'order_id',
//                'event_date',
//                'event_name',
//                'sec_name',
//                'NomBilKn',
//                'cod_sec',
//                'row',
//                'seat',
//                'price',
//                'session',
//                'ReservID',
//                'barcode',
//                'BarCodeType',
//                'reservation_exp_date',
//                'set_id',
//            ))->condition('t.order_id', $orderId)->execute();
            $ticketsResult = [[
                'id'=>10,
                'order_id'=>1,
                'event_date'=>'17-05-2015',
                'event_name'=>'event_name',
                'sec_name'=>'sec_name',
                'NomBilKn'=>3277,
                'cod_sec'=>4563,
                'row'=>4,
                'seat'=>3,
                'price'=>2500,
                'session'=>'session',
                'ReservID'=>76,
                'barcode'=>'barcode',
                'BarCodeType'=>5,
                'reservation_exp_date'=>'17-05-2015',
                'set_id'=>65
            ]];
            foreach ($ticketsResult as $ticket) {

                $key = self::getUniqueKey($ticket);
                $tickets[$key] = $ticket;
            }
            $ret = new ProfTicketOrder((array)$result, $tickets);
        }
       // dd($ret);
        return $ret;
    }

    /**
     *  Возвращает список id заказов пользователя по фильтрам
     * @param int $userId
     * @param array $sort
     * @param array $filter
     * @param array $pagination
     * @return mixed
     */
    public static function getOrdersIdsByUserId($userId, $sort = array(), $filter = array(), $pagination = array())
    {
        $query = self::getOrdersIdsQueryByUserId($userId);

        $query->leftJoin('profticket_ordered_tickets', 't', 't.order_id = o.id');
        if (!empty($filter)) {
            $query->condition($filter['field'], $filter['value']);
        }

        if (empty($sort)) {
            $sort = array(
                'field' => 'o.created',
                'dir' => 'DESC',
            );
        }

        $result = $query
            // пока пагинации нет, выводим последние заказы
            ->range(0, 30)
            ->orderBy($sort['field'], $sort['dir'])
            ->groupBy('o.id')
            ->execute()
            ->fetchAll();

        $ordersIds = array();
        foreach ($result as $orderData) {
            $ordersIds[$orderData->id] = $orderData->id;
        }
        return $ordersIds;
    }


    /**
     * Возвращает кол-во заказов пользователя по фильтру
     * @param int $userId
     * @param array $filter
     * @return int
     */
    public static function getUserOrdersCount($userId, $filter = array())
    {
        $query = self::getOrdersIdsQueryByUserId($userId);
        if (!empty($filter)) {
            $query->condition($filter['field'], $filter['value']);
        }
        $result = $query->countQuery()
            ->execute()
            ->fetchField();
        return (int) $result;
    }

    /**
     * Возвращает запрос за выборку заказов пользователя
     * @param int $userId
     * @return $this|QueryConditionInterface
     */
    public static function getOrdersIdsQueryByUserId($userId)
    {
        return db_select('profticket_order', 'o')->fields('o', array(
            'id',
        ))->condition('o.user_id', $userId);
    }

    /**
     * Сохранение заказа. Сохранение означает inser или update всех его полей и билетов.
     * В случае ошибки возвращается false, иначе true.
     * @throws ProfTicketOrderException
     * @return boolean true в случае успеха
     */
    public function save()
    {

        $transaction = db_transaction();
        try {
            if (!$this->id) {
                $orderId = db_insert('profticket_order')
                    ->fields($this->_fields)
                    ->execute();
                $this->_fields['id'] = $orderId;
            } else {
                db_update('profticket_order')->fields($this->_fields)->condition('id', $this->id)->execute();
            }

            foreach ($this->getTickets() as $ticketKey => $ticket) {
                $ticket['order_id'] = $this->id;
                if (!isset($ticket['id'])) {
                    $ticketId = db_insert('profticket_ordered_tickets')
                        ->fields($ticket)
                        ->execute();

                    // запоминаем присвоенный id
                    if ($ticketId) {
                        $this->_tickets[$ticketKey]['id'] = (int) $ticketId;
                    }
                } else {
                    $ticketId = $ticket['id'];
                    unset($ticket['id']);
                    db_update('profticket_ordered_tickets')->fields($ticket)->condition('id', $ticketId)
                        ->execute();
                }
            }
        } catch (Exception $e) {
            $transaction->rollback();
            throw new ProfTicketOrderException($e->getMessage(), $e->getCode(), $e);
        }

        return true;
    }

    /**
     * Создание заказа.
     * На вход передается:
     *
     * - массив билетов в соответствии с структурой таблицы profticket_ordered_tickets;
     * - поля заказа (e-mail, имя и телефон пользователя).
     *
     * Заказ означает необходимость оплаты. Если требуется создать бронь - использовать метод createReservation.
     *
     * В случае успеха возвращает объект заказа.
     * В случае ошибки генерируется ProfTicketOrderException
     * @param array $tickets
     * @param string $userEmail
     * @param string $userName
     * @param string $userPhone
     * @param int setId
     * @return self
     *
     * @throws ProfTicketOrderException
     */
    public static function createOrder($tickets, $userEmail, $userName, $userPhone, $setId = null)
    {
        // текущий пользователь
        global $user;

        $isSet = $setId > 0;

        $order = null;
        $transaction = db_transaction();
        try {
            $orderFields = array(
                'created' => date('Y-m-d H:i:s'),
                'reservation_id' => 0,
                'payment_status_id' => self::STATUS_NEW,
                'user_email' => $userEmail,
                'name' => $userName,
                'phone' => $userPhone,
                'tickets_sum' => 0,
                'user_id' => $user->uid ? $user->uid : null,
            );

            // записываем в профиль пользователя телефон
            // если не получится - ничего страшного
            if ($user->uid > 0) {
                try {
                    $user_fields = user_load($user->uid);

                    if (!isset($user_fields->field_phone[LANGUAGE_NONE][0]['value']) || !$user_fields->field_phone[LANGUAGE_NONE][0]['value']) {
                        $user_fields->field_phone[LANGUAGE_NONE][0]['value'] = $userPhone;
                        user_save($user_fields);
                    }

                } catch (\Exception $e) {}
            }

            //создаем заказ
            $order = new ProfTicketOrder($orderFields, array());
            $order->save();

            $client = ProfTicketGate::getInstance();

            // если комплект - нужно сгруппировать по EventsSetID
            $ticketsForReservation = array();
            if ($isSet) {
                if (!empty($tickets)) {
                    foreach ($tickets as $ticket) {
                        $setPlaceId = $ticket['EventsSetID'] . '-' . $ticket['cod_sec'] . '-' . $ticket['row'] . '-' . $ticket['seat'];
                        if (!isset($ticketsForReservation[$setPlaceId])) {
                            $ticketsForReservation[$setPlaceId] = $ticket;
                            $ticketsForReservation[$setPlaceId]['priceSell'] = 0;
                        }
                    }
                }
            } else {
                $ticketsForReservation = $tickets;
            }

            $params = self::clearParams($ticketsForReservation, array('EventsSetID', 'NomBilKn', 'cod_sec', 'row', 'seat', 'session'));
            foreach ($params as $k => $param) {
                $params[$k]['NameSpektator'] = $userName;
                $params[$k]['TelSpektator'] = $userPhone;
                $params[$k]['EmailSpektator'] = $userEmail;
                $params[$k]['Notes'] = '_';
                $params[$k]['OrderID'] = $order->id;
            }

            //бронируем
            $result = $client->call($isSet ? ProfTicketSet::SET_RESERVATION_METHOD : 'SetReservation', $params);

            $events = array();
            $tickets = array();
            $totalSum = 0;
            $reserveId = 0;
            if (empty($result)) {
                throw new ProfTicketOrderException('Error set reservation');
            }
            $_SESSION['RESERV_DATETIME'] = '';
            foreach ($result as $ticket) {
                if ($ticket['result_code'] == 0) {
                    if (!isset($events[$ticket['NomBilKn']])) {
                        $events[$ticket['NomBilKn']] =  new ProfTicketEvent($ticket['NomBilKn'], true);
                    }
                    $totalSum += (float)$ticket['priceSell'];

                    $eventTime = DateTime::createFromFormat('d.m.Y H:i:s', $events[$ticket['NomBilKn']]->EventDate . ' ' . $events[$ticket['NomBilKn']]->EventTime);
                    $dateEvent = $eventTime->format('Y-m-d H:i:s');
                    $reserveId = $ticket['ReservID'];

                    // дата истечения резерва
                    $reservationExpDate = null;
                    $reservationExpTime = strtotime($ticket['ReservDate'] . ' ' . $ticket['ReservTime']);
                    if ($reservationExpTime > 0) {
                        $reservationExpDate = date('Y-m-d H:i:s', $reservationExpTime);
                    }

                    $ticketParams = array(
                        'event_date' => $dateEvent,
                        'event_name' => $events[$ticket['NomBilKn']]->name_show,
                        'sec_name' => $ticket['name_sec'],
                        'NomBilKn' => $ticket['NomBilKn'],
                        'cod_sec' => $ticket['cod_sec'],
                        'row' => $ticket['row'],
                        'seat' => $ticket['seat'],
                        'price' => $ticket['priceSell'],
                        'reservID' => $ticket['ReservID'],
                        'session' => self::getSessionId($isSet),
                        'reservation_exp_date' => $reservationExpDate,
                        'set_id' => $setId,
                    );

                    $key = self::getUniqueKey($ticketParams);
                    $tickets[$key] = $ticketParams;

                    // запоминаем в сессии
                    if (empty($_SESSION['RESERV_DATETIME']) && $reservationExpTime > 0) {
                        $_SESSION['RESERV_DATETIME'] = date('d.m.Y, H:i', $reservationExpTime);
                    }
                } else {
                    throw new ProfTicketOrderException('Error set reservation: error code');
                }
            }

            $orderFields['id'] = $order->id;
            $orderFields['tickets_sum'] = $totalSum;
            $orderFields['reservation_id'] = $reserveId;


            $order->_tickets = $tickets;
            $order->_fields = $orderFields;
            $order->save();

            return $order;

        } catch (Exception $e) {
            $transaction->rollback();
            if ($order instanceof ProfTicketOrder) {
                $order->rollBack();
            }
            throw new ProfTicketOrderException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Возвращает id комплекта заказа
     * @return int|null
     */
    public function getSetId()
    {
        $res = null;
        if (!empty($this->_tickets)) {
            foreach ($this->_tickets as $ticket) {
                if ($ticket['set_id']) {
                    $res = $ticket['set_id'];
                    break;
                }
            }
        }

        return $res;
    }

    /**
     * Создание резерва.
     * В случае ошибки генерируется ProfTicketOrderException

     * @return self
     *
     * @throws ProfTicketOrderException
     */
    public function createReservation()
    {
        try {
            $this->_fields['payment_status_id'] = self::STATUS_RESERVATION;
            $this->save();

            //переменные шаблона
            $order = $this;
            $events = array();

            /**
             * отправка пользователю
             */
            $mail = profticket_get_mail_client();

            $mail->addAddress($this->user_email);
            $mail->isHTML(true);
            $mail->Subject = 'Заказ №' . $this->id . ' забронирован';

            ob_start();
            include __DIR__ . '/templates/mail/reserve.php';
            $message = ob_get_contents();
            ob_end_clean();
            $mail->Body = $message;

            if(!$mail->send()) {
                ProfTicketLog::log('Ошибка отправки брони заказа №' . $this->id, '', ProfTicketLog::ERROR, $this->id);
                throw new ProfTicketOrderException('Error send mail');
            }

            ProfTicketLog::log('Отправлена бронь заказа №' . $this->id, '', ProfTicketLog::INFO, $this->id);

            /**
             * отправка менеджеру
             */
            $adminsEmails = ProfTicketTools::getReserveEmails();

            $mail = profticket_get_mail_client();

            foreach ($adminsEmails as $email) {
                $mail->addAddress($email);
            }

            $mail->isHTML(true);
            $mail->Subject = 'Заказ №' . $this->id . ' забронирован';
            ob_start();
            include __DIR__ . '/templates/mail/manager_reserve.php';
            $message = ob_get_contents();
            ob_end_clean();
            $mail->Body = $message;
            $mail->send();

        } catch (Exception $e) {

            ProfTicketLog::log('Ошибка бронирования заказа №' . $this->id, '', ProfTicketLog::ERROR, $this->id);
            $this->rollBack();
            throw new ProfTicketOrderException($e->getMessage(), $e->getCode(), $e);
        }

        return $this;
    }

    /**
     * Откат заказа.
     *
     * Откат заказа необходим в случаях, если:
     * - деньги по заказу не пришли в течение какого-то времени;
     * - шлюз не принял оплату.
     *
     * Откат заказа означает:
     * 1) возврат денег в платежной системе, если был оплачен
     * 2) снятие продажи в шлюзе, если был оплачен
     * 3) снятие резерва в шюзе
     *
     * Откатить можно только тот заказ, у которого статус = STATUS_NEW или STATUS_PAID.
     * В случае ошибки генерируется ProfTicketOrderException
     *
     * @throws ProfTicketOrderException в случае ошибки.
     */
    public function rollBack()
    {
        // буудщий статус заказа
        $newOrderStatus  = self::STATUS_ROLLBACK;
        $paymentWasRevoke = false;

        ProfTicketLog::log('Пытаюсь отменить заказ №' . $this->id, null, ProfTicketLog::INFO, $this->id);

        if ($this->payment_status_id != self::STATUS_NEW
            && $this->payment_status_id != self::STATUS_PAID
            && $this->payment_status_id != self::STATUS_PENDING
            && $this->payment_status_id != self::STATUS_ERROR_SET_RETURN
            && $this->payment_status_id != self::STATUS_ERROR_SET_SOLD
            && $this->payment_status_id != self::STATUS_PAYMENT_ERROR
        ) {
            throw new ProfTicketOrderException('Error order status for rollback');
        }

        try {
            $client = ProfTicketGate::getInstance();
            $tickets = $this->getTickets();

            /**
             * возвращаем деньги, если был оплачен
             */
            if (($this->payment_status_id == self::STATUS_PAID || $this->payment_status_id == self::STATUS_ERROR_SET_SOLD)
                && $this->payment_system_order_id) {
                ProfTicketLog::log('Пытаюсь отменить оплату заказа №' . $this->id, null, ProfTicketLog::WARNING, $this->id);

                try {
                   $refundResult = ProfTicketPaymentManager::refundOrder($this);

                    // если удалось отменить оплату - уведомляем пользователя
                    if ($refundResult) {
                        $paymentWasRevoke = true;
                        ProfTicketLog::log('Отправляем письмо о возврате средств заказа №' . $this->id . ' на ' . $this->user_email, null, ProfTicketLog::WARNING, $this->id);

                        $mailMessage = variable_get('profticket_refund_mail_text', 'Возврат средств по заказу №{$orderId} выполнен, деньги вернуться на ваш счет в течение 3-х дней.');

                        $mailMessage = str_replace('{$orderId}', $this->id, $mailMessage);

                        $mail = profticket_get_mail_client();

                        $mail->addAddress($this->user_email);
                        $mail->isHTML(true);
                        $mail->Subject = 'Возврат средств по заказу №' . $this->id;
                        $mail->Body = $mailMessage;
                        $mail->send();
                    } else {
                        $newOrderStatus = self::STATUS_ERROR_ROLLBACK;
                    }
                } catch (Exception $e) {
                    throw new ProfTicketOrderException($e->getMessage(), $e->getCode(), $e);
                }

                // помечаем, что отменили оплату
                $this->payment_status_id = $newOrderStatus;
                $this->save();
            }

            /**
             * отменяем продажу в шлюзе
             * в случае ошибки продолжаем работу - пытаемся отменить резерв
             */

            if ($paymentWasRevoke || $this->payment_status_id == self::STATUS_ERROR_SET_RETURN) {
                $params = self::clearParams($tickets, array('NomBilKn', 'cod_sec', 'row', 'seat', 'session'));
                $resultReturn = $client->call('SetReturn', $params, false, $this->id);
                if (empty($resultReturn)) {
                    ProfTicketLog::log('Ошибка отмены заказа №' . $this->id . ', Ошибка установки SetReturn, пустой ответ', null, ProfTicketLog::ERROR, $this->id);
                    $newOrderStatus = self::STATUS_ERROR_SET_RETURN;
                } else {
                    foreach ($resultReturn as $returnTicket) {
                        $ticketKey = $returnTicket['cod_sec'] . '-' . $returnTicket['Row'] . '-' . $returnTicket['Seat'];
                        if ($returnTicket['result_code'] != 0) {
                            if ($this->payment_status_id == self::STATUS_ERROR_SET_RETURN) {
                                $newOrderStatus = self::STATUS_ERROR_SET_RETURN_END;
                            } else {
                                $newOrderStatus = self::STATUS_ERROR_SET_RETURN;
                            }

                            ProfTicketLog::log('Ошибка отмены заказа №' . $this->id . ', Ошибка снятия продажи для билета ' . $ticketKey, null, ProfTicketLog::ERROR, $this->id);
                        }
                    }
                }
            }

            /**
             * снимаем бронь
             */
            // для комплекта вызываем другой метод
            $setId = $this->getSetId();
            $isSet = $setId > 0;
            if ($isSet > 0) {
                $tickets = $this->getTicketsBySets();
            } else {
                $tickets = $this->getTickets();
            }

            $ticketsParams = self::clearParams($tickets, array('NomBilKn', 'cod_sec', 'row', 'seat', 'session', 'ReservID'));
            $params = array();
            foreach ($ticketsParams as $k => $ticketParam) {
                $params[$k] = $ticketParam;
                if ($setId) {
                    $params[$k]['EventsSetID'] = $setId;
                }
            }


            /**
             * Если уже был успешно выполнен SetReturn, FreeReservation вернет ошибку, т.к. в резерве билетов уже не будет
             * НЕ снимаем резерв, если отмена средств была неуспешной
             */
            if ($newOrderStatus != self::STATUS_ERROR_ROLLBACK) {
                $resultFreeReservation = $client->call($isSet ? ProfTicketSet::FREE_RESERVATION_METHOD : 'FreeReservation', $params, false, $this->id);

                foreach ($resultFreeReservation as $ticket) {
                    if ($ticket['result_code'] == 0) {
                        $newOrderStatus = self::STATUS_ROLLBACK;
                    } elseif ($this->payment_status_id == self::STATUS_ERROR_SET_RETURN) {
                        // не выходит отменить - помечаем заказ как ошибка возврата билетов
                        $newOrderStatus = self::STATUS_ERROR_SET_RETURN_END;
                    }
                }
            }

            // сохраняем изменения в билетах
            $this->cancel_date = date('Y-m-d H:i:s');
            $this->payment_status_id = $newOrderStatus;
            $this->save();

            if ($newOrderStatus == self::STATUS_ROLLBACK) {
                ProfTicketLog::log("Заказ №{$this->id} отменен", null, ProfTicketLog::INFO, $this->id);
            }

        }catch (Exception $e) {
            ProfTicketLog::log('Ошибка отмены заказа №' . $this->id . ', ' . $e->getMessage(), null, ProfTicketLog::ERROR, $this->id);
            throw new ProfTicketOrderException($e->getMessage(), $e->getCode(), $e);
        }
    }


    /**
     * Пометить билеты как оплаченные в шлюзе
     * в случае ошибки вызвать $this->rollBack
     */
    public function setSold()
    {
        /**
         * Устанавливаем ошибку продажи, чтобы узнать об ошибки в случае таймаута setSold
         */
        $this->payment_status_id = self::STATUS_ERROR_SET_SOLD;
        $this->save();
        try {
            $client = ProfTicketGate::getInstance();

            // для комплекта исользуем другой метод
            $setId = $this->getSetId();
            $isSet = $setId > 0;

            if ($isSet) {
                $ticketsForSale = $this->getTicketsBySets();
            } else {
                $ticketsForSale = $this->getTickets();
            }

            $params = self::clearParams($ticketsForSale, array('NomBilKn', 'cod_sec', 'row', 'seat', 'session'));

            if (empty($params)) {
                throw new ProfTicketOrderException('Empty tickets params');
            }

            foreach ($params as $k => $param) {
                $params[$k]['TransactionID'] = $this->payment_system_order_id;
                $params[$k]['PaymentDate'] = date('d.m.Y');
                $params[$k]['PaymentTime'] = date('H:i:s');

                if ($setId) {
                    $params[$k]['EventsSetID'] = $setId;
                }
            }

            $result = $client->call($isSet ? ProfTicketSet::SET_SOLD_METHOD : 'SetSold', $params, false, $this->id);
            if (empty($result)) {
                throw new ProfTicketOrderException('Empty set sold response');
            }

            foreach ($result as $ticket) {
                if ($ticket['result_code'] != 0) {
                    throw new ProfTicketOrderException('Error response result code');
                } else {
                    $key = self::getUniqueKey($ticket);
                    $this->_tickets[$key]['barcode'] = $ticket['barcode'];
                    $this->_tickets[$key]['BarCodeType'] = $ticket['BarCodeType'];
                }
            }

            $this->payment_status_id = self::STATUS_PAID;
            $this->payment_date = date('Y-m-d H:i:s');
            $this->save();

        } catch (\Exception $e) {
            ProfTicketLog::log('Ошибка установки продажы заказа №' . $this->id, 'SetSold', ProfTicketLog::ERROR, $this->id);
            throw $e;
        }
    }


    /**
     * Попытка предрезерва билетов и занесение их в сессию пользователя
     * на вход передается массив вида
     * Array ( [0] => 11-1-4-31 [1] => 11-1-4-30 )
     * событие-сектор-ряд-место
     * @param array $tickets
     * @param int $setId
     * @return array
     */
    public static function addTickets($tickets, $setId = null)
    {
        //сейчас у нас нет корзины, но потом быть может будет, убрать в этом случае и будет корзина
        self::freePreReservation($setId);
        $_SESSION['places'] = array();

        $toReservation = array();
        $successReserved = array();
        $failReserved = array();
        $client = ProfTicketGate::getInstance();
        $session = self::getSessionId($setId);
        //пытаемся выполнить предрезерв билетов
        foreach ($tickets as $ticket) {
            $ticket = explode('-', $ticket);
            if (count($ticket) != 4) {
                continue;
            }
            $params = array(
                'cod_sec' => $ticket[1],
                'row' => $ticket[2],
                'seat' => $ticket[3],
                'session' => $session,
            );

            // комлект
            if ($setId) {
                $params['EventsSetID'] = $ticket[0];
            } else {
                $params['NomBilKn'] = $ticket[0];
            }

            $toReservation[] = $params;

        }

        $method = $setId ? ProfTicketSet::SET_PRE_RESERVATION_METHOD : 'PreSetReservation';

        $result = $client->call($method, $toReservation);


        if (!empty($result)) {
            foreach ($result as $ticket) {
                $ticketParams = $ticket;
                $ticketParams['endReservationTime'] = date('Y-m-d H:i:s', time() + intval($ticket['TTL']));
                if ($setId) {
                    $ticketParams['EventsSetID'] = $setId;
                }
                $key = self::getUniqueKey($ticketParams);
                if ($ticket['result_code'] == 0) {
                    $successReserved[$key] = $ticketParams;
                    $_SESSION['places'][$key] = $ticketParams;
                } else {
                    $failReserved[$key] = $ticketParams;
                }
            }
        }

        return array(
            'success' => $successReserved,
            'fail' => $failReserved
        );
    }


    /**
     * Удаление билета из корзины
     */
    public static function removeTicket()
    {

    }

    /**
     * Возвращает выбранные билеты пользователем из сессии
     * @param boolean $isSet комплект
     * @return array
     */
    public static function getPreReservedTickets($isSet = null)
    {
        self::updatePreReservation($isSet);
        $result = array();
        if (!empty($_SESSION['places'])) {
            $result = $_SESSION['places'];
        }
        return $result;
    }

    /**
     * Обновление предрезерва в корзине
     * @param boolean $isSet
     * @throws Exception
     */
    public static function updatePreReservation($isSet = null)
    {
        $client = ProfTicketGate::getInstance();

        //снять с резерва
        self::freePreReservation($isSet);
        //снова поставить в резерв
        $tickets = $isSet ? self::getBasketTicketsBySets() : self::getBasketTickets();
        $params = self::clearParams($tickets, array('NomBilKn', 'EventsSetID', 'cod_sec', 'row', 'seat', 'session'));

        if (!empty($params)) {
            $result = $client->call($isSet ? ProfTicketSet::SET_PRE_RESERVATION_METHOD : 'PreSetReservation', $params);

            //удаляем из корзины те билеты, которые вернули код больше нуля
            foreach ($result as $ticket) {
                $key = self::getUniqueKey($ticket);

                if ($ticket['result_code'] != 0) {
                    unset($_SESSION['places'][$key]);
                } elseif (isset($_SESSION['places'][$key])) {
                    // обновляем время окончания резерва
                    $_SESSION['places'][$key]['endReservationTime'] = date('Y-m-d H:i:s', time() + intval($ticket['TTL']));
                }
            }
        }

    }

    /**
     * Снятие с предрезерва всех билетов из сессии
     * @param boolean $isSet
     * @throws Exception
     */
    public static function freePreReservation($isSet = null)
    {
        $client = ProfTicketGate::getInstance();
        $tickets = $isSet ? self::getBasketTicketsBySets() : self::getBasketTickets();

        if (!empty($tickets)) {
            $params = self::clearParams($tickets, array('NomBilKn', 'EventsSetID', 'cod_sec', 'row', 'seat', 'session'));
            $client->call($isSet ? ProfTicketSet::FREE_PRE_RESERVATION_METHOD : 'FreePreReservation', $params);
        }
        //обновляем сессию
        self::updateTicketSession($isSet);
    }

    /**
     * Билеты, находящиеся сейчас в корзине
     * @param boolean $isSet
     * @return array
     */
    public static function getBasketTickets($isSet = null)
    {
        $result = array();
        if (!empty($_SESSION['places'])) {
            foreach ($_SESSION['places'] as $key => $val) {
                if ($isSet && !isset($val['EventsSetID'])) {
                    continue;
                }
                if ($isSet === false && isset($val['EventsSetID']) && $val['EventsSetID']) {
                    continue;
                }

                $_SESSION['places'][$key]['session'] = self::getSessionId($isSet);
            }
            $result = $_SESSION['places'];
        }
        return $result;
    }

    /**
     * Билеты сгруппированные по комплектам в корзине
     * @return array
     */
    public static function getBasketTicketsBySets()
    {
        $tickets = self::getBasketTickets(true);
        $result = array();
        if (!empty($tickets)) {
            foreach ($tickets as $ticket) {
                $setPlaceId = $ticket['EventsSetID'] . '-' . $ticket['cod_sec'] . '-' . $ticket['row'] . '-' . $ticket['seat'];
                if (!isset($result[$setPlaceId])) {
                    $result[$setPlaceId] = $ticket;
                    $result[$setPlaceId]['priceSell'] = 0;
                    $result[$setPlaceId]['price'] = 0;
                }

                // собираем цену
                $result[$setPlaceId]['priceSell'] = (float)$result[$setPlaceId]['priceSell'] + (float)$ticket['priceSell'];
                $result[$setPlaceId]['price'] = (float)$result[$setPlaceId]['price'] + (float)$ticket['price'];
            }
        }

        return $result;
    }

    /**
     * Билеты сгруппированные по комплектам в заказе
     * @return array
     */
    public function getTicketsBySets()
    {
        $tickets = $this->_tickets;
        $result = array();
        if (!empty($tickets)) {
            foreach ($tickets as $ticket) {
                $setPlaceId = $ticket['set_id'] . '-' . $ticket['cod_sec'] . '-' . $ticket['row'] . '-' . $ticket['seat'];
                if (!isset($result[$setPlaceId])) {
                    $result[$setPlaceId] = $ticket;
                    $result[$setPlaceId]['price'] = 0;
                }

                // собираем цену
                $result[$setPlaceId]['price'] = (float)$result[$setPlaceId]['price'] + (float)$ticket['price'];
            }
        }

        return $result;
    }

    /**
     * ID сессии для покупки билетов
     * @param boolean $isSet
     * @return string
     */
    public static function getSessionId($isSet = null)
    {
        if (!isset($_SESSION['order_counter'])) {
            $_SESSION['order_counter'] = time();
        }

        /**
         * На session_id() опираться нельзя, т.к. после авторизации он изменяется
         */
        if (!isset($_SESSION['main_ticket_session_id']) || !$_SESSION['main_ticket_session_id']) {
            $_SESSION['main_ticket_session_id'] = session_id();
        }

        $ticketSessionIdMain = $_SESSION['main_ticket_session_id'];
        return md5($ticketSessionIdMain . $_SESSION['order_counter'] . ($isSet ? '_set' : ''));
    }

    /**
     * обновление сессии для билетов
     * @param boolean $isSet
     * @return string
     */
    public static function updateTicketSession($isSet = null)
    {
        $_SESSION['order_counter'] = time();
        return self::getSessionId($isSet);
    }

    /**
     * @param $arInput
     * @param $arParams
     * @return mixed
     */
    public static function clearParams($arInput, $arParams)
    {
        foreach ($arInput as $k => $val) {
            $arInput[$k] = array();
            foreach ($arParams as $paramKey) {
                $arInput[$k][$paramKey] = isset($val[$paramKey]) ? $val[$paramKey] : '';
            }
        }

        return $arInput;
    }


    /**
     * @param $ticket
     * @return string
     */
    public static function getUniqueKey($ticket)
    {
        return $ticket['NomBilKn'] . '-' . $ticket['cod_sec'] . '-' . $ticket['row'] . '-' . $ticket['seat'];
    }

    /**
     * очистка корзины
     */
    public static function clearBasket() {
        unset($_SESSION['places']);
    }

    /**
     * Получить объект билетов заказа
     * @return MPDFWrapper
     */

    public function getETicketsObject()
    {
        //отправляем только оплаченный
        if ($this->payment_status_id != self::STATUS_PAID) {
            ProfTicketLog::log('Попытка отправить билеты неоплаченного заказа №' . $this->id, '', ProfTicketLog::ERROR, $this->id);
            return false;
        }

        $mPdfWrapper = new MPDFWrapper();
        $events = array();
        // для шаблона
        $order = $this;
        foreach ($this->getTickets() as $ticket) {

            $qrCodeFile = null;
            // создаем qr code
            try {
                $qrCodeFile = $this->getQrCodePath() . ($ticket['id']) . '.png';
                QRcode::png($ticket['barcode'], $qrCodeFile, QR_ECLEVEL_L, 25, 0);
            } catch (\Exception $e) {
                ProfTicketLog::log('Ошибка создания qr-кода по заказу №' . $this->id, '', ProfTicketLog::ERROR, $this->id);
                $qrCodeFile = null;
            }


            ob_start();

            if (!isset($events[$ticket['NomBilKn']])) {
                $events[$ticket['NomBilKn']] =  new ProfTicketEvent($ticket['NomBilKn'], true);
            }
            dd(ob_get_contents());

            $event = $events[$ticket['NomBilKn']];

            //абонемент или обычное событие
            if ($event->node->type == 'events') {
                include __DIR__ . '/templates/e-ticket/e-ticket.php';
            } else {
                include __DIR__ . '/templates/e-season-ticket/e-ticket.php';
            }

            $html = ob_get_contents();
            ob_end_clean();
            $mPdfWrapper->addPage($html);

            // удаляем qr code
            if (is_file($qrCodeFile)) {
                unlink($qrCodeFile);
            }
        }
        return $mPdfWrapper;

    }

    /**
     * Создание и рассылка электронных билетов по заказу
     */
    public function sendETickets()
    {
        //отправляем только оплаченный
        if ($this->payment_status_id != self::STATUS_PAID) {
            ProfTicketLog::log('Попытка отправить билеты неоплаченного заказа №' . $this->id, '', ProfTicketLog::ERROR, $this->id);
            return false;
        }

        if ((int) $this->eticket_sent !== 0) {
            ProfTicketLog::log('Билеты уже были отправлены по заказу №' . $this->id, '', ProfTicketLog::ERROR, $this->id);
            return false;
        }

        $pdfBytes = $this->getETicketsObject()->getPdfBytes();


        $isSeason = false;

        // для шаблона
        $order = $this;
        $events = array();
        foreach ($this->getTickets() as $ticket) {
            if (!isset($events[$ticket['NomBilKn']])) {
                $events[$ticket['NomBilKn']] = new ProfTicketEvent($ticket['NomBilKn'], true);
            }
        }

        $event = reset($events);
        if ($event instanceof stdClass && $event->type != 'events') {
            $isSeason = true;
        }

        $tmpFilePath = tempnam($this->getFilesDirectory(), 'e_ticket'  . $this->id);
        file_put_contents($tmpFilePath, $pdfBytes);

        $adminsEmails = ProfTicketTools::getAdminsEmails();

        //отправка
        $mail = profticket_get_mail_client();
        $mail->addAddress($this->user_email);

        //отправляем админам
        foreach ($adminsEmails as $adminEmail) {
            $mail->addBCC($adminEmail);
        }

        $mail->addAttachment($tmpFilePath, 'e-tickets.pdf');
        $mail->isHTML(true);

        $mail->Subject = 'Заказ №' . $this->id . ' оплачен, транзакция №' . $this->payment_system_order_id;

        ob_start();
        //абонемент или обычное событие
        if ($isSeason) {
            include __DIR__ . '/templates/mail/e-season-ticket.php';
        } else {
            include __DIR__ . '/templates/mail/e-ticket.php';
        }
        $message = ob_get_contents();
        ob_end_clean();
        $mail->Body = $message;

        if(!$mail->send()) {
            ProfTicketLog::log('Ошибка отправки электронных билетов заказа №' . $this->id, '', ProfTicketLog::ERROR, $this->id);
            throw new ProfTicketOrderException('Error send tickets mail');
        }

        //Проставляем флаг отправки
        $order->eticket_sent = 1;
        $order->save();

        ProfTicketLog::log('Отправлены электронные билеты заказа №' . $this->id, '', ProfTicketLog::INFO, $this->id);
    }

    /**
     * Директория для qr кодов
     * @return bool|string
     * @throws Exception
     */
    private function getQrCodePath()
    {
        $dir = $this->getFilesDirectory() . '/qr/';
        if (!is_dir($dir)) {
            mkdir($dir, 0755);
        }
        return $dir;
    }

    /**
     * Директория хранения файлов
     * @return string
     */
    private function getFilesDirectory()
    {
        return $_SERVER['DOCUMENT_ROOT'] . '/sites/default/files/tmp';
    }

    /**
     * Директория временных файлов
     * @return string
     */
    public static function tmpDir()
    {
        return $_SERVER['DOCUMENT_ROOT'] . '/sites/default/files/tmp';
    }

    /**
     * Блокировка заказа
     * @return int|false
     */
    public function lock()
    {
        $lockFile = self::tmpDir() . '/lock_'  . $this->id;
        return file_put_contents($lockFile, time());
    }

    /**
     * Разблокировка заказа
     */
    public function unLock()
    {
        $lockFile = self::tmpDir() . '/lock_'  . $this->id;
        if (is_file($lockFile)) {
            unlink($lockFile);
        }
    }


    /**
     * Заблокирован ли заказ
     * @return bool
     */
    public function isLocked()
    {
        $lock = false;
        $lockFile = self::tmpDir() . '/lock_'  . $this->id;
        if (is_file($lockFile) && (int) file_get_contents($lockFile) > time() - 60 * 5) {
            $lock = true;
        }
        return $lock;
    }

    /**
     * Код платежной системы
     * @return string
     */
    public function getPaymentSystemCode()
    {
        return $this->payment_system_code;
    }

    /**
     * Хеш заказа
     * @return string
     */
    public function getHash()
    {
        return md5($this->id . $this->user_email . $this->created . 'df53wGfskIuv');
    }

    /**
     * Список статусов
     * @return array
     */
    public static function getStatusList()
    {
        return array(
            self::STATUS_NEW => 'Новый',
            self::STATUS_ROLLBACK => 'Отменен',
            self::STATUS_PAID => 'Оплачен',
            self::STATUS_PAYMENT_ERROR => 'Ошибка оплаты',
            self::STATUS_RESERVATION => 'Бронь',
            self::STATUS_PENDING => 'В обработке',
            self::STATUS_ERROR_ROLLBACK => 'Ошибка возврата средств',
            self::STATUS_ERROR_SET_SOLD => 'Ошибка установки продажи в шлюзе',
            self::STATUS_ERROR_SET_RETURN => 'Оплата отменена, билеты не возращены',
            self::STATUS_ERROR_SET_RETURN_END => 'Ошибка возврата билетов'
        );
    }

    /**
     * Список статусов для фронтенда
     * @return array
     */
    public static function getFrontendStatusList()
    {
        return array(
            self::STATUS_NEW => 'Забронирован',
            self::STATUS_ROLLBACK => 'Отменен',
            self::STATUS_PAID => 'Оплачен',
            self::STATUS_PAYMENT_ERROR => 'Забронирован',
            self::STATUS_RESERVATION => 'Забронирован',
            self::STATUS_PENDING => 'Забронирован',
            self::STATUS_ERROR_ROLLBACK => 'Оплачен',
            self::STATUS_ERROR_SET_SOLD => 'Ошибка',
            self::STATUS_ERROR_SET_RETURN => 'Отменен',
            self::STATUS_ERROR_SET_RETURN_END => 'Отменен'
        );
    }

    /**
     * Название статуса для фронтенда
     * @return string
     */
    public function getFrontendStatus()
    {
        $list = self::getFrontendStatusList();
        return isset($list[$this->payment_status_id]) ? $list[$this->payment_status_id] : null;
    }

    /**
     * Заносит в куки пользователю параметры оформления заказа
     * чтобы после авторизации\регистрации можно было продолжить заказ
     * @param int $eventId
     * @param bool $isReservation
     * @param bool $isSet
     */
    public static function setNoAuthStartOrderParams($eventId, $isReservation, $isSet = false)
    {
        $params = array(
            'eventId' => $eventId,
            'isReservation' => $isReservation,
            'isSet' => $isSet
        );
        setcookie('start_order_params', json_encode($params), time() + 60 * 60, '/');
    }

    /**
     * Удаляет куку с параметрами оформления заказа
     */
    public static function unsetNoAuthStartOrderParams()
    {
        setcookie('start_order_params', '', time() - 60 * 60, '/');
    }

    /**
     * Параметры начала оформления заказа
     * @return string|null
     */
    public static function getNoAuthStartOrderParams()
    {
        return isset($_COOKIE['start_order_params']) && $_COOKIE['start_order_params']
            ? json_decode($_COOKIE['start_order_params'], true) : null;
    }
}

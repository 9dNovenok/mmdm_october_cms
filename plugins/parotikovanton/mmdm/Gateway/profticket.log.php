<?php
/**
 * Класс для работы с логами
 */
class ProfTicketLog
{

    const INFO = 0;
    const WARNING = 1;
    const ERROR = 2;

    /**
     * Логирование
     * @param int $level
     * @param string $message
     * @param string $method
     * @param int $orderId
     */
    public static function log($message, $method = null, $level = 0, $orderId = null)
    {
        if (defined('PROFTICKET_LOG_LEVEL') && PROFTICKET_LOG_LEVEL <= $level) {
            $fields = array(
                'level' => $level,
                'method' => $method,
                'message' => $message,
                'order_id' => $orderId,
                'date' => date('Y-m-d H:i:s')
            );
            db_insert('profticket_log')->fields($fields)->execute();
        }
    }
}

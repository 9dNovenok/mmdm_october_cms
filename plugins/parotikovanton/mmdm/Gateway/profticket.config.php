<?php
/**
 * Конфигурация модуля ProfTicket
 *
 * Здесь указываются:
 * 1) параметры подключения к шлюзу;
 * 2) параметры доступа к платежной системе Platron.
 */
$localConfig = __DIR__ . '/profticket.config.local.php';
if (file_exists($localConfig)) {
    require_once $localConfig;
} else {
    define('PROFTICKET_WSDL', 'http://217.70.30.166:8150/test/ProfticketGate.exe/wsdl/IProfTicket');
    define('PROFTICKET_USER', 'GateTest');
    define('PROFTICKET_PASSWORD', 'G@te123');

    //уровень логирования, если не определено - логирования нет
    define('PROFTICKET_LOG_LEVEL', 0);

    //почта администаторов
    define('PROFTICKET_ADMIN_EMAIL', 'sysadmin@internet-design.ru, chukina@internet-design.ru');
    define('PROFTICKET_FROM_EMAIL', 'info@mmdm.ru');


    //Platron - платёжная система
    $platronConfig = array();
    $platronConfig['pg_merchant_id'] = '9234';
    $platronConfig['secretKey'] = 'hovagupaxateruha';
    $platronConfig['pg_testing_mode'] = '1';
    $platronConfig['pay_url'] = 'http://www.platron.ru/payment.php';
    $platronConfig['status_url'] = 'http://www.platron.ru/get_status.php';
    $platronConfig['pg_result_url'] = 'http://mmdm.local/pay-result.php';
    $platronConfig['pg_revoke_url'] = 'http://www.platron.ru/revoke.php';
    $platronConfig['pg_request_method'] = 'GET';
    $platronConfig['pg_success_url'] = 'http://mmdm.local/success-order/';
    $platronConfig['pg_failure_url'] = 'http://mmdm.local/fail-order/';
    $platronConfig['pg_success_url_method'] = 'AUTOGET';
    $platronConfig['pg_failure_url_method'] = 'AUTOGET';


    // CloudPayments - платежная система
    $cloudPaymentsConfig = array();
    $cloudPaymentsConfig['login'] = 'pk_20b4287c831d3db977ff2f459fb1e';
    $cloudPaymentsConfig['secret'] = 'ac0bc3ec5a5d9f3fe8d5238c563623eb';
    $cloudPaymentsConfig['success_url'] = 'http://mmdm.local/success-order/';
    $cloudPaymentsConfig['failure_url'] = 'http://mmdm.local/fail-order/';
    $cloudPaymentsConfig['3d_secure_url'] = 'http://mmdm.local/payment/cloud/check-3d-secure.php';

    // массив всех настроек платежных систем
    $paymentSystemsConfig = array(
        'Platron' => $platronConfig,
        'CloudPayments' => $cloudPaymentsConfig,
    );
    unset($platronConfig);
    unset($cloudPaymentsConfig);
}


<?php


/**
 * Обертка для mdf
 */
class MPDFWrapper
{
    private $mpdf;

    public function __construct($debug = false)
    {
        require_once(__DIR__ . "/mpdf/mpdf.php");
        $this->mpdf = new mPDFClass('', 'A4', 0, 'Arial', 0, 0, 0, 0, 0, 0, 'P');
        $this->mpdf->SetDisplayMode('fullpage');

        //$this->mpdf->SetBasePath()
        $this->mpdf->dpi = 300;
        $this->mpdf->img_dpi = 300;

        if ($debug) {
            $this->mpdf->debug = true;
            $this->mpdf->showImageErrors = true;
        }
    }

    public function addPage($html)
    {
        $this->mpdf->AddPage();
        $this->mpdf->WriteHTML($html);
    }

    /**
     * @return string pdf bytes
     */
    public function getPdfBytes()
    {
        return $this->mpdf->Output(null, 'S');
    }

    /**
     *
     */
    public function output()
    {
        return $this->mpdf->Output();
    }
}

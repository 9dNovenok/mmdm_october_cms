<?php
// массив с запросами для upgrad'а до нужной версии БД
$upgradeVersions = array(

    // создать таксономию для списка залов
    1 => 'create_hall_list_taxonomy',

    // создать поле для связки мероприятий
    2 => 'create_event_external_id_field',

    // создание таблицы для хранения заказов
    3 => array(
        "CREATE TABLE `profticket_order` (
            `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор заказа',
            `created` DATETIME NOT NULL COMMENT 'Дата создания заказа',
            `reservation_id` INT (11) NOT NULL COMMENT 'Номер резерва в ProfTicket',
            `tickets_sum` FLOAT(9, 2) NOT NULL COMMENT 'Стоимость заказанных билетов',
            `payment_status_id` SMALLINT NOT NULL COMMENT 'Статус платежа',
            `payment_system_order_id` VARCHAR(255) NULL COMMENT 'Номер заказа в платежной системе',
            `payment_transaction_id` VARCHAR(255) NULL COMMENT 'Номер транзакции в платежной системе',
            `payment_date` DATETIME NULL COMMENT 'Дата оплаты',
            `user_email` VARCHAR(255) NOT NULL COMMENT 'E-mail пользователя',
            `name` VARCHAR(255) NOT NULL COMMENT 'Имя пользователя',
            `phone` VARCHAR(255) NULL COMMENT 'Телефон пользователя',
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Таблица заказов пользователей'",

        "CREATE TABLE `profticket_ordered_tickets` (
            `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор заказа',
            `order_id` INT (11) NOT NULL COMMENT 'Идентификтор заказа из таблицы profticket_order',
            `event_date` DATETIME NOT NULL COMMENT 'Дата проведения мероприятия',
            `event_name` VARCHAR (255) NOT NULL COMMENT 'Название мероприятия',
            `sec_name` VARCHAR (255) NOT NULL COMMENT 'Название сектора в ProfTicket',
            `NomBilKn` INT (11) NOT NULL COMMENT 'Идентификатор события в ProfTicket',
            `cod_sec` INT (11) NOT NULL COMMENT 'Идентификатор сектора в ProfTicket',
            `row` VARCHAR (10) NOT NULL COMMENT 'Номер ряда',
            `seat` VARCHAR (10) NOT NULL COMMENT 'Номер места',
            `price` FLOAT (9, 2) NOT NULL COMMENT 'Стоимость билета',
            `session` VARCHAR(32) NOT NULL COMMENT 'Идентификатор сессии',
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Таблица заказанных билетов'
        ",

        "ALTER TABLE `profticket_ordered_tickets` ADD FOREIGN KEY (`order_id`) REFERENCES `profticket_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE",
    ),
    4 => array(
        "CREATE TABLE `profticket_log` (
            `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT '',
            `date` DATETIME NOT NULL COMMENT 'Дата',
            `level` INT (11) NOT NULL DEFAULT 0 COMMENT 'Уровень критичности',
            `order_id` INT (11) NULL COMMENT 'Идентификтор заказа из таблицы profticket_order',
            `method` VARCHAR(125) NULL COMMENT 'Метод',
            `message` text NOT NULL COMMENT 'Сообщение',
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Логи работы модуля profticket'
        "
    ),

    5 => array(
        'ALTER TABLE `profticket_ordered_tickets` ADD `reservID` INT (11) NOT NULL ;'
    ),

    6 => array(
        'ALTER TABLE `profticket_ordered_tickets` ADD `barcode` VARCHAR (125) NOT NULL default \'\';',
        'ALTER TABLE `profticket_ordered_tickets` ADD `BarCodeType`  VARCHAR (125) NOT NULL default \'\';'
    ),
    7 => array(
        'ALTER TABLE `profticket_order` ADD `payment_system_code` VARCHAR (125) NOT NULL default \'\';',
    ),
    8 => array(
        //Были ли отправлены письма по заказу, прошлым проставим 1
        'ALTER TABLE `profticket_order` ADD `eticket_sent` TINYINT (1) NOT NULL default 0;',
        'UPDATE `profticket_order` SET `eticket_sent`=1 WHERE `payment_status_id`=2;'
    ),
    9 => array(
        'ALTER TABLE `profticket_order` ADD `user_id` int null;',
        'ALTER TABLE `profticket_order` ADD INDEX `order__user_id` (`user_id`)',
    ),
    10 => array(
        'ALTER TABLE `profticket_ordered_tickets` ADD `reservation_exp_date` DATETIME NULL;',
    ),
    11 => array(
        'ALTER TABLE `profticket_order` ADD `cancel_date` DATETIME NULL;',
    ),
    12 => array(
        'ALTER TABLE `profticket_ordered_tickets` ADD `set_id` int null;',
    ),
    13 => array(
        "CREATE TABLE `profticket_event` (
            `event_id` INT (11) NOT NULL COMMENT 'id события',
            `date` DATETIME NOT NULL COMMENT 'Дата события',
            `free_places_count` INT (11) NOT NULL default 0 COMMENT 'кол-во свободных мест',
            `e_ticket_permitted` tinyint(1) NOT NULL default 0 COMMENT 'разрешена продажа',
            `updated` tinyint(1) NOT NULL default 1 COMMENT 'разрешена продажа',
            PRIMARY KEY (`event_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Информация о событиях из шлюза'
        "
    ),
);

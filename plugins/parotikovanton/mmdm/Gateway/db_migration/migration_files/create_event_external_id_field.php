<?php
/**
 * Создание поля для внешнего кода мероприятия в ProfTicket
 */

$field = field_info_field('event_external_id');

if ($field == null) {
    // поле еще не существует
    $field = array(
        'field_name' => 'event_external_id',
        'type' => 'number_integer',
        'cardinality' => 1,
        'settings' => array(),
        'entity_types' => array('node'),
    );
    field_create_field($field);
}

// привязка поля к типу метариала
$instances = field_info_instances('node', 'events');

if (!isset($instances['event_external_id'])) {
    $instance = array(
        'field_name' => 'event_external_id',
        'entity_type' => 'node',
        'bundle' => 'events',
        'label' => 'Код события в ProfTicket',
        'description' => 'Номер события в ProfTicket. Не путать событие с мероприятием',
        'weight' => 1,
        'widget' => array(
            'type' => 'number',
            'label' => 'Код события в ProfTicket',
        )
    );
    $field = field_create_instance($instance);
}
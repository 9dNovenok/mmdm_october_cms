<?php
/**
 * Создание словаря для залов
 */

$vocName = 'hall';

$vocabulary = taxonomy_vocabulary_machine_name_load($vocName);
if (!$vocabulary) {
    $vocabulary = array(
        'machine_name' => $vocName,
        'name' => 'Залы',
    );

    $vocabulary = (object) $vocabulary;
    taxonomy_vocabulary_save($vocabulary);
}

$field = field_info_field('hall_external_id');

if ($field == null) {
    $field = array(
        'field_name' => 'hall_external_id',
        'type' => 'number_integer',
        'cardinality' => 1, /* not necessary as it's the default.*/
        'settings' => array(),
        'entity_types' => array('taxonomy_term'),
    );
    field_create_field($field);
}

$instance = array(
    'field_name' => 'hall_external_id',
    'entity_type' => 'taxonomy_term',
    'bundle' => $vocName,
    'label' => 'Код зала в ProfTicket',
    'weight' => 1,
    'widget' => array(
        'type' => 'number',
        'label' => 'Код зала в ProfTicket',
    ),
);
$field = field_create_instance($instance);

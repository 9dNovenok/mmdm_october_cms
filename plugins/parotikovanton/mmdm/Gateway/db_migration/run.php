<?php
/**
 * Скрипт для внесения изменений в базу данных
 * запуск из командной строки
 * php -f run.php
 */

// подключить Drupal
define('DRUPAL_ROOT', realpath(__DIR__ . '/../../../../../'));

$_SERVER['DOCUMENT_ROOT'] = DRUPAL_ROOT;
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

chdir(DRUPAL_ROOT);

require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// создать таблицу, если ее еще нет
db_query('CREATE TABLE IF NOT EXISTS svk_db_version_info (version INT NOT NULL)')->execute();

require_once __DIR__ . '/upgrade.cfg.php';

echo "Database update\n";

// получить текущую версию
$currentVersion = (int) db_query('SELECT version FROM svk_db_version_info')->fetchColumn();
if (!trim($currentVersion)) {
    db_insert('svk_db_version_info')->fields(array('version' => 0))->execute();
    $currentVersion = 0;
}

$currentVersion = (int) $currentVersion;

echo "Current db version is: $currentVersion\n";

end($upgradeVersions);
$newVersion = key($upgradeVersions);

echo "New db version is: $newVersion\n";

if ($currentVersion != $newVersion) {
    echo "Current DB version is out of date, updating DB structure...\n";
    foreach ($upgradeVersions as $k=>$v) {
        if ($k > $currentVersion) {
            echo "Upgrading to version $k\n";

            $isVersionError = false;

            $transaction = db_transaction();

            if (is_array($v)) {
                foreach ($v as $sql) {
                    echo "SQL: $sql\n";
                    try {
                        if (db_query($sql)) {
                            echo "SUCCESS\n";
                        }
                    }
                    catch (Exception $ex) {
                        echo 'ERROR ' . $ex->getMessage() . "\n";
                        $isVersionError = true;
                    }
                    if ($isVersionError) {
                        break;
                    }
                }
            }
            else if (is_file(__DIR__ . '/migration_files/' . $v . '.php')) {
                try {
                    echo "FILE: $v\n";
                    include __DIR__ . '/migration_files/' . $v . '.php';
                    echo "SUCCESS\n";
                } catch (Exception $ex) {
                    echo 'ERROR ' . $ex->getMessage() . "\n";
                    $isVersionError = true;
                }
            }

            if ($isVersionError) {
                // откат транзакции
                $transaction->rollback();
                exit();
            }
            else {
                // коммит транзакции
                unset ($transaction);
                db_update('svk_db_version_info')->fields(array('version' => $k))->execute();
            }
        }
    }
}
else {
    echo "Current DB version is up to date\n";
}

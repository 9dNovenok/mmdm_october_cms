<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmEvents16 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->boolean('showfestival')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->dropColumn('showfestival');
        });
    }
}

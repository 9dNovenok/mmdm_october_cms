<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateParotikovantonMmdmEvents extends Migration
{
    public function up()
    {
        Schema::create('parotikovanton_mmdm_events', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->dateTime('data_event')->nullable();
            $table->integer('type_of_event')->nullable();
            $table->integer('hall_full')->nullable();
            $table->integer('age_rating')->nullable();
            $table->text('nazvanie_test')->nullable();
            $table->text('title')->nullable();
            $table->text('title_long')->nullable();
            $table->text('tekst_dlja_listinga')->nullable();
            $table->text('image')->nullable();
            $table->text('short_text')->nullable();
            $table->text('long_text')->nullable();
            $table->text('programm_short')->nullable();
            $table->text('programm_long')->nullable();
            $table->text('text_announcement')->nullable();
            $table->text('body')->nullable();
            $table->boolean('remove_sale')->nullable();
            $table->text('video_event')->nullable();
            $table->integer('tags')->nullable();
            $table->integer('upcoming_events')->nullable();
            $table->text('path')->nullable();
            $table->boolean('checkbox_sticky_big')->nullable();
            $table->boolean('repertoire')->nullable();
            $table->integer('other_events')->nullable();
            $table->integer('language')->nullable();
            $table->integer('external_id')->nullable();
            $table->text('img_slide')->nullable();
            $table->boolean('stock')->nullable();
            $table->integer('stock_url')->nullable();
            $table->text('price_stock')->nullable();
            $table->text('infi_stock')->nullable();
            $table->text('ticket_image')->nullable();
            $table->boolean('child')->nullable();
            $table->time('duration')->nullable();
            $table->integer('organizer_id')->nullable();
            $table->text('organizer_name')->nullable();
            $table->text('organizer_tin')->nullable();
            $table->text('organizer_address')->nullable();
            $table->integer('changes_event_list')->nullable();
            $table->text('changes_event')->nullable();
            $table->time('event_time')->nullable();
            $table->text('metatags')->nullable();
            $table->text('cycles_name')->nullable();
            $table->boolean('cycles')->nullable();
            $table->text('img_cycles')->nullable();
            $table->text('janer_cycles')->nullable();
            $table->text('aksia_sikli')->nullable();
            $table->text('batten_sele')->nullable();
            $table->text('batten_sele_in')->nullable();
            $table->text('onclick_buy')->nullable();
            $table->text('onclick_bron')->nullable();
            $table->text('programm')->nullable();
            $table->boolean('sold')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('parotikovanton_mmdm_events');
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateParotikovantonMmdmNews extends Migration
{
    public function up()
    {
        Schema::create('parotikovanton_mmdm_news', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title_ru')->nullable();
            $table->string('title_en')->nullable();
            $table->text('content_ru')->nullable();
            $table->text('content_en')->nullable();
            $table->text('anons_ru')->nullable();
            $table->text('anons_en')->nullable();
            $table->text('images')->nullable();
            $table->dateTime('data')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('parotikovanton_mmdm_news');
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmEvents13 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->text('participant_ru')->nullable();
            $table->text('participant_en')->nullable();
            $table->text('program_ru')->nullable();
            $table->text('program_en')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->dropColumn('participant_ru');
            $table->dropColumn('participant_en');
            $table->dropColumn('program_ru');
            $table->dropColumn('program_en');
        });
    }
}

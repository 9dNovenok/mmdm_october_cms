<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateParotikovantonMmdmBanners extends Migration
{
    public function up()
    {
        Schema::create('parotikovanton_mmdm_banners', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('img_url')->nullable();
            $table->text('href')->nullable();
            $table->text('alt')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('parotikovanton_mmdm_banners');
    }
}

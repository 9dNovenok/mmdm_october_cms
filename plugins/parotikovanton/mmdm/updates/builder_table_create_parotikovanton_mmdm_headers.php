<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateParotikovantonMmdmHeaders extends Migration
{
    public function up()
    {
        Schema::create('parotikovanton_mmdm_headers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('header')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('parotikovanton_mmdm_headers');
    }
}

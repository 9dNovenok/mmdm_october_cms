<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmFestivals2 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_festivals', function($table)
        {
            $table->boolean('active')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_festivals', function($table)
        {
            $table->dropColumn('active');
        });
    }
}

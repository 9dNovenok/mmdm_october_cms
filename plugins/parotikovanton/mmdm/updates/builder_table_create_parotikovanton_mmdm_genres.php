<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateParotikovantonMmdmGenres extends Migration
{
    public function up()
    {
        Schema::create('parotikovanton_mmdm_genres', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name_ru')->nullable();
            $table->string('name_en')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('parotikovanton_mmdm_genres');
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmNews2 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_news', function($table)
        {
            $table->string('slug')->nullable();
            $table->integer('order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_news', function($table)
        {
            $table->dropColumn('slug');
            $table->dropColumn('order');
        });
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmHeaders extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_headers', function($table)
        {
            $table->text('header_ru')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->renameColumn('header', 'header_en');
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_headers', function($table)
        {
            $table->dropColumn('header_ru');
            $table->increments('id')->unsigned()->change();
            $table->renameColumn('header_en', 'header');
        });
    }
}

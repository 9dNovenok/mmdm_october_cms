<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmFooter extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_footer', function($table)
        {
            $table->text('menu_ru')->nullable();
            $table->text('copyrighted_ru')->nullable();
            $table->text('menu_en')->nullable();
            $table->text('copyrighted_en')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->dropColumn('menu');
            $table->dropColumn('copyrighted');
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_footer', function($table)
        {
            $table->dropColumn('menu_ru');
            $table->dropColumn('copyrighted_ru');
            $table->dropColumn('menu_en');
            $table->dropColumn('copyrighted_en');
            $table->increments('id')->unsigned()->change();
            $table->text('menu')->nullable();
            $table->text('copyrighted')->nullable();
        });
    }
}

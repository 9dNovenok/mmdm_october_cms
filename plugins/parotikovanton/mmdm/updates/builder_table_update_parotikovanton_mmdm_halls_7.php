<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmHalls7 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_halls', function($table)
        {
            $table->string('seo_title_ru')->nullable();
            $table->string('seo_desc_ru')->nullable();
            $table->string('seo_key_ru')->nullable();
            $table->string('seo_title_en')->nullable();
            $table->string('seo_desc_en')->nullable();
            $table->string('seo_key_en')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_halls', function($table)
        {
            $table->dropColumn('seo_title_ru');
            $table->dropColumn('seo_desc_ru');
            $table->dropColumn('seo_key_ru');
            $table->dropColumn('seo_title_en');
            $table->dropColumn('seo_desc_en');
            $table->dropColumn('seo_key_en');
        });
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmEvents6 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->text('main_img')->nullable();
            $table->text('images')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->dropColumn('main_img');
            $table->dropColumn('images');
        });
    }
}

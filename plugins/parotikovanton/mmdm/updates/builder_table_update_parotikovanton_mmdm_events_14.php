<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmEvents14 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->text('changes_event_en')->nullable();
            $table->renameColumn('changes_event', 'changes_event_ru');
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->dropColumn('changes_event_en');
            $table->renameColumn('changes_event_ru', 'changes_event');
        });
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmNews3 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_news', function($table)
        {
            $table->string('seo_title_ru')->nullable();
            $table->string('seo_title_en')->nullable();
            $table->string('seo_key_ru')->nullable();
            $table->string('seo_key_en')->nullable();
            $table->text('seo_desc_ru')->nullable();
            $table->text('seo_desc_en')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_news', function($table)
        {
            $table->dropColumn('seo_title_ru');
            $table->dropColumn('seo_title_en');
            $table->dropColumn('seo_key_ru');
            $table->dropColumn('seo_key_en');
            $table->dropColumn('seo_desc_ru');
            $table->dropColumn('seo_desc_en');
        });
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateParotikovantonMmdmFooter extends Migration
{
    public function up()
    {
        Schema::create('parotikovanton_mmdm_footer', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('banners')->nullable();
            $table->text('socials')->nullable();
            $table->text('menu')->nullable();
            $table->text('copyrighted')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('parotikovanton_mmdm_footer');
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmEvents5 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->string('title_ru')->nullable();
            $table->string('title_en')->change();
            $table->dropColumn('tittle_ru');
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->dropColumn('title_ru');
            $table->string('title_en', 191)->change();
            $table->string('tittle_ru', 191)->nullable();
        });
    }
}

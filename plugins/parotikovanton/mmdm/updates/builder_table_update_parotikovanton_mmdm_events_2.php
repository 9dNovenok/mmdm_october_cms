<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmEvents2 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->dropColumn('deleted_at');
        });
    }
}

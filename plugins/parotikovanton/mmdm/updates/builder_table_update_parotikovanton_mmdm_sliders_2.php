<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmSliders2 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_sliders', function($table)
        {
            $table->text('slide_ru')->nullable();
            $table->text('slide_en')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_sliders', function($table)
        {
            $table->dropColumn('slide_ru');
            $table->dropColumn('slide_en');
        });
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateParotikovantonMmdmSlider extends Migration
{
    public function up()
    {
        Schema::create('parotikovanton_mmdm_slider', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('slider')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('parotikovanton_mmdm_slider');
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmHalls3 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_halls', function($table)
        {
            $table->string('title_ru')->nullable();
            $table->string('title_en')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_halls', function($table)
        {
            $table->dropColumn('title_ru');
            $table->dropColumn('title_en');
        });
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateParotikovantonMmdmHalls extends Migration
{
    public function up()
    {
        Schema::create('parotikovanton_mmdm_halls', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('content_ru')->nullable();
            $table->text('content_en')->nullable();
            $table->text('images')->nullable();
            $table->text('documents')->nullable();
            $table->string('tour')->nullable();
            $table->string('scheme_img');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('parotikovanton_mmdm_halls');
    }
}

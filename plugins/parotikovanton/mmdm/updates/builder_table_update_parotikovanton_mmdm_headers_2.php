<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmHeaders2 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_headers', function($table)
        {
            $table->text('phone')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_headers', function($table)
        {
            $table->dropColumn('phone');
        });
    }
}

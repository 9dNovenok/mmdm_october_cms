<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteParotikovantonMmdmFooterBanners extends Migration
{
    public function up()
    {
        Schema::dropIfExists('parotikovanton_mmdm_footer_banners');
    }
    
    public function down()
    {
        Schema::create('parotikovanton_mmdm_footer_banners', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('image')->nullable();
            $table->text('href')->nullable();
            $table->text('alt')->nullable();
        });
    }
}

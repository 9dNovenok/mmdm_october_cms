<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmFestivals3 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_festivals', function($table)
        {
            $table->text('content_ru')->nullable();
            $table->text('anons_ru')->nullable();
            $table->text('content_en')->nullable();
            $table->text('anons_en')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_festivals', function($table)
        {
            $table->dropColumn('content_ru');
            $table->dropColumn('anons_ru');
            $table->dropColumn('content_en');
            $table->dropColumn('anons_en');
        });
    }
}

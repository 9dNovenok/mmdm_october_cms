<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateParotikovantonMmdmFooterBanners extends Migration
{
    public function up()
    {
        Schema::create('parotikovanton_mmdm_footer_banners', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('image')->nullable();
            $table->text('href')->nullable();
            $table->text('alt')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('parotikovanton_mmdm_footer_banners');
    }
}

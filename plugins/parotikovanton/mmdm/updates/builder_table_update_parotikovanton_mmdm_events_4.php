<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmEvents4 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->text('content_en')->nullable();
            $table->text('content_ru')->nullable();
            $table->string('tittle_ru')->nullable();
            $table->string('title_en')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_events', function($table)
        {
            $table->dropColumn('content_en');
            $table->dropColumn('content_ru');
            $table->dropColumn('tittle_ru');
            $table->dropColumn('title_en');
        });
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmHalls4 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_halls', function($table)
        {
            $table->string('main_img')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_halls', function($table)
        {
            $table->dropColumn('main_img');
        });
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmHalls6 extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_halls', function($table)
        {
            $table->integer('order')->nullable()->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_halls', function($table)
        {
            $table->dropColumn('order');
        });
    }
}

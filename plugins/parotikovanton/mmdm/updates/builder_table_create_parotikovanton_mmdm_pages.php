<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateParotikovantonMmdmPages extends Migration
{
    public function up()
    {
        Schema::create('parotikovanton_mmdm_pages', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('slug')->nullable();
            $table->text('content_ru')->nullable();
            $table->text('content_en')->nullable();
            $table->text('seo_title_ru')->nullable();
            $table->text('seo_title_en')->nullable();
            $table->text('seo_desc_ru')->nullable();
            $table->text('seo_desc_en')->nullable();
            $table->text('seo_key_ru')->nullable();
            $table->text('seo_key_en')->nullable();
            $table->text('title_ru')->nullable();
            $table->text('title_en')->nullable();
            $table->text('active')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('parotikovanton_mmdm_pages');
    }
}

<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmSliders extends Migration
{
    public function up()
    {
        Schema::rename('parotikovanton_mmdm_slider', 'parotikovanton_mmdm_sliders');
        Schema::table('parotikovanton_mmdm_sliders', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::rename('parotikovanton_mmdm_sliders', 'parotikovanton_mmdm_slider');
        Schema::table('parotikovanton_mmdm_slider', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}

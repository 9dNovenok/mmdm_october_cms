<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmFestivals extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_festivals', function($table)
        {
            $table->string('title_ru')->nullable();
            $table->string('title_en')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->string('image')->change();
            $table->string('slug')->change();
            $table->dropColumn('title');
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_festivals', function($table)
        {
            $table->dropColumn('title_ru');
            $table->dropColumn('title_en');
            $table->increments('id')->unsigned()->change();
            $table->string('image', 191)->change();
            $table->string('slug', 191)->change();
            $table->string('title', 191)->nullable();
        });
    }
}

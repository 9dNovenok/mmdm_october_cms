<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmNews extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_news', function($table)
        {
            $table->boolean('active')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->string('title_ru')->change();
            $table->string('title_en')->change();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_news', function($table)
        {
            $table->dropColumn('active');
            $table->increments('id')->unsigned()->change();
            $table->string('title_ru', 191)->change();
            $table->string('title_en', 191)->change();
        });
    }
}

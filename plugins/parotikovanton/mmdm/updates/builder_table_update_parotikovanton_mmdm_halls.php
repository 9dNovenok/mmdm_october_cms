<?php namespace Parotikovanton\Mmdm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateParotikovantonMmdmHalls extends Migration
{
    public function up()
    {
        Schema::table('parotikovanton_mmdm_halls', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->string('tour')->change();
            $table->string('scheme_img')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('parotikovanton_mmdm_halls', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->string('tour', 191)->change();
            $table->string('scheme_img', 191)->nullable(false)->change();
        });
    }
}

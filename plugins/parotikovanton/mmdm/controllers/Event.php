<?php namespace Parotikovanton\Mmdm\Controllers;

use Backend\Classes\Controller;
use Parotikovanton\Mmdm\Models\Event as EventModel;
use BackendMenu;

class Event extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'all' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Parotikovanton.Mmdm', 'events_controlls', 'events');
    }
    static public function saveEvents($events)
    {
        $result = collect();
        $events->map(function ($event) use ($result){
            collect([
                "NomBilKn" => "190",
                "EventDate" => "19.09.2018",
                "EventTime" => "19:00:00",
                "BeginDate" => "19.09.2018",
                "BeginTime" => "19:00:00",
                "withOpenDate" => "0",
                "StopOrderDate" => "19.09.2018",
                "StopOrderTime" => "19:00",
                "StopSaleDate" => "19.09.2018",
                "StopSaleTime" => "19:00",
                "cod_show" => "439",
                "cod_t" => "407",
                "cod_h" => "647",
                "name_t" => "Московский международный Дом музыки",
                "name_h" => "Камерный зал",
                "EventDuration" => "01:00:00",
                "is_Primera" => "0",
                "EventNote" => "",
                "name_show" => "Балет-детям 1 серия ( 13 час.)",
                "author" => "",
                "Note1" => "",
                "Note2" => "",
                "Note3" => "",
                "Note4" => "",
                "Age" => "0",
                "Producer" => "",
                "Actors" => "",
                "Annotation" => "",
                "WithIntermisson" => "0",
                "MinPrice" => "1.00",
                "MaxPrice" => "8.00",
                "MinPriceSell" => "1.00",
                "MaxPriceSell" => "8.00",
                "FreePlacesQty" => "546",
                "FreePlacesTicketOffice" => "546",
                "FreePlacesEventsSet" => "546",
                "isFreeDelivery" => "0",
                "OrganizerID" => "31",
                "OrganizerName" => "ООО Национальное Концертное Агентство",
                "OrganizerTin" => "7713766979",
                "OrganizerAddress" => "127247, г. Москва, Бескудниковский бульвар, д.35",
                "ETicketPermitted" => "1",
                "SeasonTicket" => "0",
                "SeasonTicketId" => "0",
                "SeasonTicketEvents" => "0",
                "EventsSetID" => "6",
                "EventsSetEvents" => "3",
                "EventsSetName" => "Балет детям",
                "MuliSelectAllowed" => "0",
                "DeleteOrderPlacesPermitted" => "0",
                "isService" => "0",
                "isCanceled" => "0",
                "LinkedServicesExists" => "0",
                "PromoExists" => "0",
                "isShowBySectors" => "0",
                "isRevaluation" => "0",
                "isReservPermitted" => "1",
                "isSalesPermitted" => "1",
                "result_code" => "0",
                "result_message" => "OK"
            ]);
            $result->push(EventModel::updateorcreate(['NomBilKn'],$event));
        });
        return $result;
    }
}

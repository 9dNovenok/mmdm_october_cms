<?php namespace Parotikovanton\Mmdm\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Hall extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'all' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Parotikovanton.Mmdm', 'events_controlls', 'halls');
    }
}

<?php namespace Parotikovanton\Mmdm\Models;

use Model;

/**
 * Model
 */
class Hall extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;
    public $jsonable = ['documents','images'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'parotikovanton_mmdm_halls';
}

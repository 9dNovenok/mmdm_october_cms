<?php namespace Parotikovanton\Mmdm\Models;

use Model;

/**
 * Model
 */
class Header extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;
    public $jsonable = ['header_ru','header_en' , 'phone'];
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'parotikovanton_mmdm_headers';
}

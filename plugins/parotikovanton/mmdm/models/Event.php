<?php namespace Parotikovanton\Mmdm\Models;

use Model;

/**
 * Model
 */
class Event extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $jsonable = ['images'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'parotikovanton_mmdm_events';


    public $belongsTo = [
        'hall'    => ['Parotikovanton\Mmdm\Models\Hall'],
        'festival'=> ['Parotikovanton\Mmdm\Models\Festival']
    ];




}
